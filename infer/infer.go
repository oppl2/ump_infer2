/*
 * @Author: Yinjie Lee
 * @Date: 2022-09-10 12:53:44
 * @LastEditors: Yinjie Lee
 * @LastEditTime: 2022-09-20 17:14:03
 */
package infer

import (
	"strconv"
	"strings"
	"time"

	fu "udp_infer2/frameutils"
	uic "udp_infer2/frameutils/upimgcmd"
	. "udp_infer2/logger"

	"github.com/pebbe/zmq4"
	"github.com/sirupsen/logrus"
)

const ENDPOINT string = "ipc:///root/tmp/sinserver.ipc"

// const ENDPOINT string = "ipc:///home/virtual-liu/tmp/sinserver.ipc"

var zmq *zmq4.Socket

func init() {
	err := zeromqClient()
	if err != nil {
		Logger.WithField("ENDPOINT", ENDPOINT).Errorln("init zeromq failed", err)
	}
}

func zeromqClient() error {
	zctx, err := zmq4.NewContext()
	if err != nil {
		return err
	}
	s, err := zctx.NewSocket(zmq4.REQ)
	if err != nil {
		return err
	}
	// err = s.Connect(ENDPOINT)
	err = s.Bind(ENDPOINT)
	if err != nil {
		return err
	}
	zmq = s
	return nil
}

type Result struct {
	res     string
	running bool
}

var imgQueue = make(map[uint32]*uic.Recvb)
var imgResult = make(map[uint32]*Result)

func PushImg(imgID uint32, data *uic.Recvb) {
	var maxId uint32 = 0xFFFFFFFF
	var key uint32
	if len(imgQueue) > 10 {
		for k := range imgQueue {
			if k < maxId {
				key = k
			}
		}
		if _, ok := imgQueue[key]; ok {
			delete(imgQueue, key)
		}
	}
	if src, ok := imgQueue[imgID]; ok {
		if err := src.Merge(data); err != nil {
			Logger.Errorln(err)
		}
		delete(imgResult, imgID)
	}
	imgQueue[imgID] = data
	// 若存在，清理图像结果缓存
	delete(imgResult, imgID)
}

func MergeImg(imgID uint32, data *uic.Recvb) {
	src, ok := imgQueue[imgID]
	if !ok {
		Logger.WithField("imgID", imgID).Errorln("not found img cache")
	}
	src.GetImgBodyBytes() // TOCHECK
}

func pushResult(imgID uint32, res *Result) {
	var maxId uint32 = 0xFFFFFFFF
	var key uint32
	if len(imgResult) > 10 {
		for k := range imgResult {
			if k < maxId {
				key = k
			}
		}
		if _, ok := imgResult[key]; ok {
			delete(imgResult, key)
		}
	}
	imgResult[imgID] = res
}

func GetResultValue(imgID uint32) ([]float32, bool) {
	v, ok := imgResult[imgID]
	if !ok {
		return []float32{0}, false
	}
	return getInferResults(v.res), ok
}

func CheckInferReady(imgid uint32) (uint32, bool) {
	img, ok := imgQueue[imgid]
	if !ok {
		Logger.WithFields(logrus.Fields{
			"imgID":    imgid,
			"imgQueue": imgQueue,
		}).Errorln("未查找到图像数据")
		return fu.NotFoundDataId, false
	}
	l := img.GetMissingLen()
	if l > 100 {
		Logger.WithField("img missing package num", l).Errorln("该图片数据丢失过多")
		return fu.LargeLostPacketRatio, false
	}
	// TODO :检查模型文件等操作
	return 0, true
}

func GetInferStatus(imgID uint32) bool {
	v, ok := imgResult[imgID]
	if !ok {
		return false
	}
	return v.running
}

func getInferResults(re string) []float32 {
	res_string := strings.Fields(re)
	nb := len(res_string)
	Logger.Debugln(" nb = ", nb/6)
	res := make([]float32, nb)
	for i := 0; i < nb; i++ {
		v, _ := strconv.ParseFloat(res_string[i], 32)
		res[i] = float32(v)
	}
	return res
}

func GetImgValue(imgID uint32) (v *uic.Recvb, ok bool) {
	v, ok = imgQueue[imgID]
	return
}

func RunInfer(imgid uint32) (res string, err error) {
	start := time.Now()
	img := imgQueue[imgid]
	sp := time.Now()
	imgData := img.GetImgData()
	Logger.WithField("time", time.Since(sp)).Debug("拼接图像")
	r := new(Result)
	pushResult(imgid, r)
	r.running = true
	defer func() {
		r.running = false
	}()
	comT := time.Now()
	_, err = zmq.SendBytes(imgData[img.GetImgDefaultDataSize():img.GetImgWidth()*img.GetImgHeight()+img.GetImgDefaultDataSize()], 0)
	if err != nil {
		Logger.Errorln(err)
		return
	}
	r.res, err = zmq.Recv(0)
	if err != nil {
		Logger.Errorln(err)
	}
	Logger.WithField("time", time.Since(comT)).Debugln("接收推理结果")

	Logger.WithField("time", time.Since(start)).Debugln("推理结束")
	// 存储执行结果
	return
}
