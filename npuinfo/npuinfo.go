/*
 * @Author: Yinjie Lee
 * @Date: 2022-09-13 21:13:28
 * @LastEditors: Yinjie Lee
 * @LastEditTime: 2022-09-14 11:18:33
 */
package npuinfo

/*
#include <stdio.h>
#include <dcmi_interface_api.h>
#cgo LDFLAGS: -L/usr/local/Ascend/driver/lib64 -ldcmi

int npuinfo_init(){
    return dcmi_init();
}

int npuinfo_temperature()
{
    int ret = 0;
    int card_id = 0;
    int device_id = 0;
    int temperature = 0;

    ret = dcmi_get_device_temperature(card_id, device_id, &temperature);
    if (ret != 0) {
        return ret;
    }
    return temperature;
}
*/
import "C"

func NpuInit() int {
	return int(C.npuinfo_init())
}

func NpuTemperature() int {
	C.npuinfo_temperature()
	return int(C.npuinfo_temperature())
}
