#! /bin/bash
###
 # @Author: Yinjie Lee
 # @Date: 2022-09-21 16:42:48
 # @LastEditors: Yinjie Lee
 # @LastEditTime: 2022-09-21 21:10:53
###
# 执行
# ./zip.sh udp_infer2.ota
file="$1"

./zip "$file"
zip udp_infer.zip "$file" "${file}.checksum"

