// package main client test
package main

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"image/color"
	"image/jpeg"
	"io"
	"io/ioutil"
	"log"
	"net"
	"os"
	"reflect"
	"time"

	fu "udp_infer2/frameutils"
	. "udp_infer2/logger"

	"github.com/sirupsen/logrus"
)

var (
	ip_add = net.IPv4(192, 168, 110, 190)
	// ip_add = net.IPv4(0, 0, 0, 0)
	// ip_add = net.IPv4(192, 168, 137, 190)
	// ip_add = net.IPv4(192, 168, 2, 3)
)

const (
	bytesSize        = 1012
	default_size     = 990
	bin_default_size = 980
	PORT             = 37009
)

var commandBeg uint32 = 0x1234

func ackCheckSumCmp(frame []byte, code uint32) {
	prepare_checksum := fu.GetChecksumByte4(code) +
		fu.GetChecksumByte4(fu.RepCodeStatusValue(frame)) +
		fu.GetChecksumByte4(fu.RepCountNumValue(frame)) +
		fu.GetChecksumByte(fu.RepBodyValue(frame))

	if fu.RepCheckSumValue(frame) != prepare_checksum {
		Logger.WithFields(logrus.Fields{
			"prepare_checksum": prepare_checksum,
			"recv_checksum":    fu.RepCheckSumValue(frame),
			"frame":            frame,
		}).Panicln("ask checksum 校验失败")
	}
}

func replyCheckSumCmp(frame []byte, code uint32) {
	prepare_checksum := fu.GetChecksumByte4(code) +
		fu.GetChecksumByte4(fu.RepCodeStatusValue(frame)) +
		fu.GetChecksumByte4(fu.RepCountNumValue(frame)) +
		fu.GetChecksumByte(fu.RepBodyValue(frame))

	if fu.RepCheckSumValue(frame) != prepare_checksum {
		Logger.WithFields(logrus.Fields{
			"prepare_checksum": prepare_checksum,
			"recv_checksum":    fu.RepCheckSumValue(frame),
		}).Panicln("reply checksum 校验失败")
	}
}

func codeCountCpm(frame []byte, codeCount uint32) {
	replpCodeCount := fu.RepCountNumValue(frame)
	if replpCodeCount != codeCount {
		Logger.WithFields(logrus.Fields{
			"response code count": codeCount,
			"reply code count":    replpCodeCount,
		}).Panicln("指令计数校验错误")
	}
}

func throughImgIDCmp(frame []byte, imgID uint32) {
	replyBody := fu.RepBodyValue(frame)
	_ = replyBody[3]
	replyImgID := uint32(replyBody[0]) | uint32(replyBody[1])<<8 | uint32(replyBody[2])<<16 | uint32(replyBody[3])<<24
	if replyImgID != imgID {
		Logger.WithFields(logrus.Fields{
			"reply img ID":    replyImgID,
			"response img ID": imgID,
		}).Panicln("img ID 校验错误")
	}
}

func throughImgPackageNum(frame []byte, imgPackageNum uint16) {
	replyBody := fu.RepBodyValue(frame)
	_ = replyBody[5]
	replyPackageNum := uint16(replyBody[4]) | uint16(replyBody[5])<<8
	if replyPackageNum != imgPackageNum {
		Logger.WithFields(logrus.Fields{
			"reply package nums":    replyPackageNum,
			"response package nums": imgPackageNum,
		}).Panicln("图像包总数 校验错误")
	}
}

var missUDPPackage = make(map[uint16][]byte, 0)

func nextID(body []byte) uint16 {
	_ = body[1]
	return uint16(body[0]) | uint16(body[1])<<8
}

var retry = true

func udpMissing(socket *net.UDPConn, frame []byte, nb_packets int) {
	replyBody := fu.RepBodyValue(frame)
	_ = replyBody[5]
	udpMissingnum := uint16(replyBody[6]) | uint16(replyBody[7])<<8
	if udpMissingnum > 100 {
		Logger.WithFields(logrus.Fields{
			"丢包数大于100": udpMissingnum,
		}).Errorln("丢包数 校验失败")
	}
	if udpMissingnum < 100 && retry {
		retry = false
		Logger.WithFields(logrus.Fields{
			"丢包数小于100": udpMissingnum,
		}).Debugln("图像包总数")
		body := fu.RepBodyValue(frame)
		missStart := body[8:]
		missOffset := 8
		for i := 0; i < int(udpMissingnum); i++ {
			pid := nextID(missStart[missOffset:])
			socket.Write(missUDPPackage[pid])
			Logger.Debugln("重传package", uint16(pid)+uint16(i))
			missOffset += 2
		}
		recv_query(socket, nb_packets)
	}
}

func throughModuleID(frame []byte, moduleID uint32) {
	replyBody := fu.RepBodyValue(frame)
	_ = replyBody[5]
	replyModuleID := uint32(replyBody[4]) | uint32(replyBody[5])<<8 | uint32(replyBody[6])<<16 | uint32(replyBody[7])<<24
	if replyModuleID != moduleID {
		Logger.WithFields(logrus.Fields{
			"reply module ID":    replyModuleID,
			"response module ID": moduleID,
		}).Panicln("模型ID 校验错误")
	}
}

func infRunCondCmp(frame []byte) {
	replyBody := fu.RepBodyValue(frame)
	_ = replyBody[8]
	replyCond := uint32(replyBody[8]) | uint32(replyBody[9])<<8 | uint32(replyBody[10])<<16 | uint32(replyBody[11])<<24
	if replyCond != 0x33 {
		Logger.WithFields(logrus.Fields{
			"reply infer run cond": fmt.Sprintf("0x%X", replyCond),
			"expect value":         fmt.Sprintf("0x%X", 0x33),
		}).Panicln("启动条件返回错误")
	}
}

func upgradeRunCondCmp(frame []byte) {
	replyBody := fu.RepBodyValue(frame)
	_ = replyBody[15]
	replyCond := uint32(replyBody[12]) | uint32(replyBody[13])<<8 | uint32(replyBody[14])<<16 | uint32(replyBody[15])<<24
	reason := uint32(replyBody[16]) | uint32(replyBody[17])<<8 | uint32(replyBody[18])<<16 | uint32(replyBody[19])<<24
	if replyCond != 0x33 {
		Logger.WithFields(logrus.Fields{
			"reply infer run cond": fmt.Sprintf("0x%X", replyCond),
			"expect value":         fmt.Sprintf("0x%X", 0x33),
			"reason ":              fmt.Sprintf("0x%X", reason),
		}).Errorln("启动条件返回错误")
	}
}

func systeminfo_print(f []byte) {
	body := fu.RepBodyValue(f)
	fmt.Println(body)
	Logger.WithFields(logrus.Fields{
		"操作系统版本":   fmt.Sprintf("0x%X", binary.LittleEndian.Uint32(body[0:4])),
		"软件版本号":    fmt.Sprintf("0x%X", binary.LittleEndian.Uint32(body[4:8])),
		"已运行时间":    binary.LittleEndian.Uint32(body[8:12]),
		"自检结果":     fmt.Sprintf("0x%X", binary.LittleEndian.Uint16(body[12:14])),
		"AI状态":     fmt.Sprintf("0x%X", binary.LittleEndian.Uint16(body[14:16])),
		"npu温度":    binary.LittleEndian.Uint32(body[16:20]),
		"标签个数":     body[20],
		"标签内容1":    body[21],
		"标签内容2":    body[22],
		"标签内容3":    body[23],
		"标签内容4":    body[24],
		"标签内容5":    body[25],
		"标签内容6":    body[26],
		"标签内容7":    body[27],
		"标签内容8":    body[28],
		"标签内容9":    body[29],
		"标签内容10":   body[30],
		"标签训练情况1":  body[31],
		"标签训练情况2":  body[32],
		"标签训练情况3":  body[33],
		"标签训练情况4":  body[34],
		"标签训练情况5":  body[35],
		"标签训练情况6":  body[36],
		"标签训练情况7":  body[37],
		"标签训练情况8":  body[38],
		"标签训练情况9":  body[39],
		"标签训练情况10": body[40],
	}).Debugln("系统信息")
}

func systeminfo_que(socket *net.UDPConn) {
	commandBeg++
	codeCount := commandBeg
	tosend := make([]byte, 1024)
	binary.LittleEndian.PutUint32(tosend[:4], uint32(0x55AA55AA))
	binary.LittleEndian.PutUint32(tosend[4:8], uint32(bytesSize))
	binary.LittleEndian.PutUint32(tosend[8:12], uint32(0x4501))
	binary.LittleEndian.PutUint32(tosend[12:16], 0)                 // tmp
	binary.LittleEndian.PutUint32(tosend[16:20], uint32(codeCount)) // command counter
	// Body
	tosend[20] = 3 // label nums
	tosend[21] = 1
	tosend[22] = 2
	tosend[23] = 3

	idx := 1020
	check_sum := GetChecksumByte(tosend[8:idx])
	binary.LittleEndian.PutUint32(tosend[idx:idx+4], check_sum)
	socket.Write(tosend)
	time.Sleep(time.Millisecond * 4)

	aaa := make([]byte, 2048)
	recvbuf := bytes.NewBuffer(aaa)
	socket.Read(recvbuf.Bytes())
	ackCheckSumCmp(recvbuf.Bytes(), 0x4501)
	codeCountCpm(recvbuf.Bytes(), codeCount)

	socket.Read(recvbuf.Bytes())
	replyCheckSumCmp(recvbuf.Bytes(), 0x4501)
	codeCountCpm(recvbuf.Bytes(), codeCount)
	systeminfo_print(recvbuf.Bytes())

}

func prepare_up_image(socket *net.UDPConn, w, h int32, nb_packets int) {
	commandBeg++
	codeCount := commandBeg
	tosend := make([]byte, 1024)
	binary.LittleEndian.PutUint32(tosend[:4], uint32(0x55AA55AA))
	binary.LittleEndian.PutUint32(tosend[4:8], uint32(bytesSize))
	binary.LittleEndian.PutUint32(tosend[8:12], uint32(0x4502))
	binary.LittleEndian.PutUint32(tosend[12:16], 0)                 // tmp
	binary.LittleEndian.PutUint32(tosend[16:20], uint32(codeCount)) // command counter
	// Body
	binary.LittleEndian.PutUint32(tosend[20:24], uint32(imgId)) // img id
	binary.LittleEndian.PutUint16(tosend[24:26], uint16(w))
	binary.LittleEndian.PutUint16(tosend[26:28], uint16(h))
	binary.LittleEndian.PutUint16(tosend[28:30], uint16(1))
	binary.LittleEndian.PutUint16(tosend[30:32], uint16(default_size))
	binary.LittleEndian.PutUint16(tosend[32:34], uint16(nb_packets))
	// binary.LittleEndian.PutUint32(tosend[28:32], uint32(nb_packets))
	// binary.LittleEndian.PutUint32(tosend[32:36], uint32(1))

	idx := 1020
	check_sum := GetChecksumByte(tosend[8:idx])
	binary.LittleEndian.PutUint32(tosend[idx:idx+4], check_sum)
	time.Sleep(time.Millisecond * 8)
	socket.Write(tosend)

	aaa := make([]byte, 2048)
	recvbuf := bytes.NewBuffer(aaa)
	socket.Read(recvbuf.Bytes())
	ackCheckSumCmp(recvbuf.Bytes(), 0x4502)
	codeCountCpm(recvbuf.Bytes(), codeCount)

	socket.Read(recvbuf.Bytes())
	replyCheckSumCmp(recvbuf.Bytes(), 0x4502)
	codeCountCpm(recvbuf.Bytes(), codeCount)

}

func upimg(socket *net.UDPConn, left_bytes int, nb_packets int, dataBytes []byte) {
	start := time.Now()
	for i := 0; i < nb_packets; i++ {
		commandBeg++
		codeCount := commandBeg
		cur_len := default_size
		if left_bytes < cur_len {
			cur_len = left_bytes
		}
		// tosend := nil
		tosend := make([]byte, 1024)
		binary.LittleEndian.PutUint32(tosend[:4], uint32(0x55AA55AA))
		binary.LittleEndian.PutUint32(tosend[4:8], uint32(bytesSize))
		binary.LittleEndian.PutUint32(tosend[8:12], uint32(0x4503))
		binary.LittleEndian.PutUint32(tosend[12:16], 0)                 // tmp
		binary.LittleEndian.PutUint32(tosend[16:20], uint32(codeCount)) // command counter

		// img id
		binary.LittleEndian.PutUint32(tosend[20:24], uint32(imgId)) // 4 bytes
		//
		binary.LittleEndian.PutUint16(tosend[24:26], uint16(nb_packets)) // 2 bytes

		binary.LittleEndian.PutUint16(tosend[26:28], uint16(i+1)) // 2 bytes
		binary.LittleEndian.PutUint16(tosend[28:30], uint16(cur_len))
		// beg := 24 + 96
		beg := 4*5 + (4 + 2*3)
		copy(tosend[beg:beg+default_size], dataBytes[i*default_size:i*default_size+cur_len])

		check_sum := GetChecksumByte(tosend[8:1020])
		// fmt.Print(" check_sum = ", check_sum)

		idx := 1020
		// logrus.Infoln("packetid =", i, " : ", len(tosend))
		binary.LittleEndian.PutUint32(tosend[idx:idx+4], check_sum)
		// time.Sleep(time.Microsecond * 3)
		// frameUtils.PrintBuf(fmt.Sprintf("packaget %d [%d] tosend =", i+1, len(tosend)), tosend)
		socket.Write(tosend)
		missUDPPackage[uint16(i+1)] = tosend
		// udpinfer.Send(ip_add+":37009", "1", tosend)
		// udpinfer.Send("192.168.137.200:37009", "1", tosend)
		// udpinfer.Send(ip_add+":37009", "1", tosend)
		left_bytes = left_bytes - cur_len
		tosend = nil
	}
	Logger.Debugln("aaaa time", time.Since(start))
}

func recv_query(socket *net.UDPConn, nb_packets int) {
	commandBeg++
	codeCount := commandBeg
	tosend := make([]byte, 1024)
	binary.LittleEndian.PutUint32(tosend[:4], uint32(0x55AA55AA))
	binary.LittleEndian.PutUint32(tosend[4:8], uint32(bytesSize))
	binary.LittleEndian.PutUint32(tosend[8:12], uint32(0x4504))
	binary.LittleEndian.PutUint32(tosend[12:16], 0)
	binary.LittleEndian.PutUint32(tosend[16:20], uint32(codeCount)) // command counter

	// img id
	binary.LittleEndian.PutUint32(tosend[20:24], uint32(imgId))
	binary.LittleEndian.PutUint16(tosend[24:26], uint16(nb_packets))
	check_sum := GetChecksumByte(tosend[8:1020])
	binary.LittleEndian.PutUint32(tosend[1020:1024], check_sum)

	aaa := make([]byte, 2048)
	recvbuf := bytes.NewBuffer(aaa)
	// fmt.Println(tosend)
	socket.Write(tosend)
	select {
	case <-time.After(time.Millisecond * 5):
		panic("timeout")
	default:
		socket.Read(recvbuf.Bytes())
	}
	ackCheckSumCmp(recvbuf.Bytes(), 0x4504)
	codeCountCpm(recvbuf.Bytes(), codeCount)

	socket.Read(recvbuf.Bytes())
	replyCheckSumCmp(recvbuf.Bytes(), 0x4504)
	codeCountCpm(recvbuf.Bytes(), codeCount)
	throughImgIDCmp(recvbuf.Bytes(), imgId)
	throughImgPackageNum(recvbuf.Bytes(), uint16(nb_packets))
	udpMissing(socket, recvbuf.Bytes(), nb_packets)
}

func exec_infer(socket *net.UDPConn, nb_packets int) {
	commandBeg++
	codeCount := commandBeg
	tosend := make([]byte, 1024)
	binary.LittleEndian.PutUint32(tosend[:4], uint32(0x55AA55AA))
	binary.LittleEndian.PutUint32(tosend[4:8], uint32(bytesSize))
	binary.LittleEndian.PutUint32(tosend[8:12], uint32(0x4505))
	binary.LittleEndian.PutUint32(tosend[12:16], 0) // tmp
	binary.LittleEndian.PutUint32(tosend[16:20], codeCount)
	// img id
	binary.LittleEndian.PutUint32(tosend[20:24], uint32(imgId))
	binary.LittleEndian.PutUint32(tosend[24:28], uint32(0x00001000))

	check_sum := GetChecksumByte(tosend[8:1020])
	binary.LittleEndian.PutUint32(tosend[1020:1020+4], check_sum)

	time.Sleep(time.Microsecond * 8)
	socket.Write(tosend)
	time.Sleep(time.Millisecond * 200)
	aaa := make([]byte, 2048)
	recvbuf := bytes.NewBuffer(aaa)
	socket.Read(recvbuf.Bytes())
	ackCheckSumCmp(recvbuf.Bytes(), 0x4505)
	codeCountCpm(recvbuf.Bytes(), codeCount)

	socket.Read(recvbuf.Bytes())
	Logger.Debugln("reason", fu.RepBodyValue(recvbuf.Bytes())[12:100])
	replyCheckSumCmp(recvbuf.Bytes(), 0x4505)
	codeCountCpm(recvbuf.Bytes(), codeCount)
	throughImgIDCmp(recvbuf.Bytes(), imgId)
	throughModuleID(recvbuf.Bytes(), uint32(0x00001000))
	infRunCondCmp(recvbuf.Bytes())
}

func infer_query(socket *net.UDPConn, nb_packets int) {
	commandBeg++
	codeCount := commandBeg
	tosend := make([]byte, 1024)
	binary.LittleEndian.PutUint32(tosend[:4], uint32(0x55AA55AA))
	binary.LittleEndian.PutUint32(tosend[4:8], uint32(bytesSize))
	binary.LittleEndian.PutUint32(tosend[8:12], uint32(0x4506))
	binary.LittleEndian.PutUint32(tosend[12:16], 0) // tmp
	infc := uint32(codeCount)
	binary.LittleEndian.PutUint32(tosend[16:20], infc)

	// img id
	binary.LittleEndian.PutUint32(tosend[20:24], uint32(imgId))
	binary.LittleEndian.PutUint32(tosend[24:28], uint32(0x00001000))
	idx := 1020
	check_sum := GetChecksumByte(tosend[8:idx])
	binary.LittleEndian.PutUint32(tosend[idx:idx+4], check_sum)
	// time.Sleep(InferDelay)
	aaa := make([]byte, 2048)
	recvbuf := bytes.NewBuffer(aaa)
	time.Sleep(time.Microsecond * 50)
	socket.Write(tosend)
	socket.Read(recvbuf.Bytes())
	ackCheckSumCmp(recvbuf.Bytes(), 0x4506)
	codeCountCpm(recvbuf.Bytes(), codeCount)

	socket.Read(recvbuf.Bytes())
	Logger.Debugln("reason", fu.RepBodyValue(recvbuf.Bytes())[12:100])
	replyCheckSumCmp(recvbuf.Bytes(), 0x4506)
	codeCountCpm(recvbuf.Bytes(), codeCount)
	throughImgIDCmp(recvbuf.Bytes(), imgId)
	throughModuleID(recvbuf.Bytes(), uint32(0x00001000))
}

func prepare_upgrade_bin(socket *net.UDPConn, total_len uint32) {
	commandBeg++
	codeCount := commandBeg
	tosend := make([]byte, 1024)
	binary.LittleEndian.PutUint32(tosend[:4], uint32(0x55aa55aa))
	binary.LittleEndian.PutUint32(tosend[4:8], uint32(bytesSize))
	binary.LittleEndian.PutUint32(tosend[8:12], uint32(0x4507))
	// skip 4 bytes
	binary.LittleEndian.PutUint32(tosend[16:20], uint32(codeCount)) // command counter

	binary.LittleEndian.PutUint32(tosend[20:24], uint32(0xaa))
	binary.LittleEndian.PutUint32(tosend[24:28], uint32(total_len))

	idx := 1020
	check_sum := GetChecksumByte(tosend[8:idx])
	binary.LittleEndian.PutUint32(tosend[idx:idx+4], check_sum)
	socket.Write(tosend)
	time.Sleep(time.Millisecond * 8)
	aaa := make([]byte, 2048)
	recvbuf := bytes.NewBuffer(aaa)
	socket.Read(recvbuf.Bytes())
	ackCheckSumCmp(recvbuf.Bytes()[:1024], 0x4507)
	codeCountCpm(recvbuf.Bytes()[:1024], codeCount)
	socket.Read(recvbuf.Bytes())
	ackCheckSumCmp(recvbuf.Bytes()[:1024], 0x4507)
	codeCountCpm(recvbuf.Bytes()[:1024], codeCount)
}

func upgrade_bin(socket *net.UDPConn, tarBytes []byte, total_len int, nb_packets int) {
	idx := 1020
	left_bytes := total_len
	aaa := make([]byte, 2048)
	for i := 0; i < (nb_packets); i++ { // nb_packets
		commandBeg++
		codeCount := commandBeg
		cur_len := bin_default_size
		if left_bytes < cur_len {
			cur_len = left_bytes
		}
		tosend := make([]byte, 1024)
		binary.LittleEndian.PutUint32(tosend[:4], uint32(0x55aa55aa))
		binary.LittleEndian.PutUint32(tosend[4:8], uint32(bytesSize))
		binary.LittleEndian.PutUint32(tosend[8:12], uint32(0x4508))
		binary.LittleEndian.PutUint32(tosend[12:16], 0) // tmp
		// skip 4 bytes
		binary.LittleEndian.PutUint32(tosend[16:20], uint32(codeCount))
		// body
		binary.LittleEndian.PutUint32(tosend[20:24], uint32(0xaa))
		binary.LittleEndian.PutUint32(tosend[24:28], uint32(total_len))
		binary.LittleEndian.PutUint32(tosend[28:32], uint32(nb_packets))
		binary.LittleEndian.PutUint32(tosend[32:36], uint32(i+1))
		binary.LittleEndian.PutUint32(tosend[36:40], uint32(cur_len))
		// Logger.Debugln(total_len, nb_packets, i+1, cur_len)
		beg := 40

		// if i != nb_packets-1 {
		// 	copy(tosend[beg:beg+bin_default_size], tarBytes[i*bin_default_size:i*bin_default_size+cur_len])
		// 	check_sum := GetChecksumByte(tosend[8:idx])
		// 	binary.LittleEndian.PutUint32(tosend[idx:idx+4], check_sum)
		// }
		copy(tosend[beg:beg+bin_default_size], tarBytes[i*bin_default_size:i*bin_default_size+cur_len])
		check_sum := GetChecksumByte(tosend[8:idx])
		binary.LittleEndian.PutUint32(tosend[idx:idx+4], check_sum)

		time.Sleep(60 * time.Microsecond)
		// fmt.Println(tosend[20:], tosend[24:28])
		socket.Write(tosend)
		left_bytes = left_bytes - cur_len
		tosend = nil
		recvbuf := bytes.NewBuffer(aaa)
		socket.Read(recvbuf.Bytes())
		ackCheckSumCmp(recvbuf.Bytes(), 0x4508)
		codeCountCpm(recvbuf.Bytes(), codeCount)
	}
}

func upgrade_query(socket *net.UDPConn, nb_packets uint32, total_len int) {
	// / upgrade query
	commandBeg++
	codeCount := commandBeg
	idx := 1020
	tosend := make([]byte, 4*6+1000)
	binary.LittleEndian.PutUint32(tosend[:4], uint32(0x55aa55aa))
	binary.LittleEndian.PutUint32(tosend[4:8], uint32(bytesSize))
	binary.LittleEndian.PutUint32(tosend[8:12], uint32(0x4509))
	binary.LittleEndian.PutUint32(tosend[12:16], uint32(nb_packets+1))
	binary.LittleEndian.PutUint32(tosend[16:20], uint32(codeCount)) // command counter
	//body
	binary.LittleEndian.PutUint32(tosend[20:24], uint32(0xaa))
	binary.LittleEndian.PutUint32(tosend[24:28], uint32(total_len))
	binary.LittleEndian.PutUint32(tosend[28:32], uint32(nb_packets))
	check_sum := GetChecksumByte(tosend[8:idx])
	binary.LittleEndian.PutUint32(tosend[idx:idx+4], check_sum)
	time.Sleep(1 * time.Millisecond)
	socket.Write(tosend)

	aaa := make([]byte, 2048)
	recvbuf := bytes.NewBuffer(aaa)
	socket.Read(recvbuf.Bytes())
	ackCheckSumCmp(recvbuf.Bytes(), 0x4509)
	codeCountCpm(recvbuf.Bytes(), codeCount)

	socket.Read(recvbuf.Bytes())
	replyCheckSumCmp(recvbuf.Bytes(), 0x4509)
	codeCountCpm(recvbuf.Bytes(), codeCount)
	upgradeRunCondCmp(recvbuf.Bytes())
}

func getPixels(file io.Reader, isGetOneChannel bool) ([][]byte, int32, int32, error) {
	img, err := jpeg.Decode(file)
	fmt.Println("img type : ", reflect.TypeOf(img))
	if err != nil {
		return nil, 0, 0, err
	}

	bounds := img.Bounds()
	width, height := bounds.Max.X, bounds.Max.Y
	if isGetOneChannel {
		width = 1792
		height = 1792
	}
	var pixels [][]byte
	for y := 0; y < height; y++ {
		var row []byte
		for x := 0; x < width; x++ {
			if c, ok := img.At(x, y).(color.YCbCr); ok {
				r, g, b := color.YCbCrToRGB(c.Y, c.Cb, c.Cr)
				// fmt.Print(r, g, b, "\n")
				// row = row + " " + strconv.Itoa(int(r))
				// row = append(row, r, g, b)
				if isGetOneChannel {
					row = append(row, r)
				} else {
					row = append(row, r, g, b)
				}
			} else {
				fmt.Println(reflect.TypeOf(img.At(x, y)))
			}
		}
		pixels = append(pixels, row)
	}
	// sep := []byte("")
	// res := bytes.Join(pixels, sep)
	// return res, int32(width), int32(height), nil
	// res := strings.Join(pixels, " ")

	return pixels, int32(width), int32(height), nil
}

func readImage(imgFn string, isGetOneChannel bool) ([][]byte, int32, int32, error) {
	file, err := os.Open(imgFn)

	pixels, w, h, err := getPixels(file, isGetOneChannel)
	defer file.Close()

	if err != nil {
		fmt.Println("Error: Image could not be decoded")
		os.Exit(1)
	}

	// fmt.Println(len(pixels))
	return pixels, w, h, nil
}

var imgId uint32 = 0x123456

func writeAll(filename string, data []byte) error {
	err := os.WriteFile(filename, data, 0666)
	if err != nil {
		return err
	}
	return nil
}

func GenerateUpgradeFile(path string) {
	f, err := os.Open(path)
	if err != nil {
		// 打开文件失败
		log.Fatal(err)
	}
	var data []byte
	buf := make([]byte, 1024)
	t1 := time.Now()
	for {
		// 将文件中读取的byte存储到buf中
		n, err := f.Read(buf)
		if err != nil && err != io.EOF {
			log.Fatal(err)
		}
		if n == 0 {
			break
		}
		// 将读取到的结果追加到data切片中
		data = append(data, buf[:n]...)
	}
	// 将data切片转为字符串即使文件内容
	// fmt.Println(string(data))
	t2 := time.Now()
	// 计算读取文件的耗时
	fmt.Println(t2.Sub(t1))

	var cur_checksum = make([]byte, 4)
	binary.LittleEndian.PutUint32(cur_checksum, GetChecksumByte(data))

	writeAll("./udp_infer2.checksum", cur_checksum)
}

func GetChecksumByte(data []byte) uint32 {
	csum := uint32(0)
	length := len(data)
	for i := 0; i < length; i++ { // 4 Bytes
		csum += uint32(data[i])
	}
	return uint32(csum)
}

func test_send_file(filepath string) {
	socket, err := net.DialUDP("udp", nil, &net.UDPAddr{
		IP:   ip_add,
		Port: PORT,
	})
	if err != nil {
		fmt.Println("conect udp server failed,err: ", err)
		return
	}
	defer socket.Close()

	tarBytes, err := ioutil.ReadFile(filepath)
	if err != nil {
		fmt.Println(err)
	}
	total_len := len(tarBytes) // with 总校验
	nb_packets := 3139
	// nb_packets := int((total_len + 490) / 980)
	// totalChecksum := GetChecksumByte(tarBytes[:total_len-4])

	fmt.Println("\ntotal_len = ", total_len, "\t nb_packets = ", nb_packets, "\n")
	// 4507
	prepare_upgrade_bin(socket, uint32(total_len))

	// 4508
	upgrade_bin(socket, tarBytes, total_len, nb_packets)
	time.Sleep(time.Second * 3)
	// upgrade query
	upgrade_query(socket, uint32(nb_packets), total_len)
	// udpinfer.Send(ip_add+":37009", "1", tosend)
}

func test_send_img_file_bytes(filepath string, isGetOneChannel bool, imgId uint32) {
	socket, err := net.DialUDP("udp", nil, &net.UDPAddr{
		IP:   ip_add,
		Port: PORT,
	})
	if err != nil {
		fmt.Println("conect udp server failed,err: ", err)
		return
	}
	defer socket.Close()

	imgBytes, w, h, err := readImage(filepath, isGetOneChannel)
	if err != nil {
		fmt.Println(err, w, h)
	}
	sep := []byte("")
	dataBytes := bytes.Join(imgBytes, sep)
	if err != nil {
		log.Fatal("Failed to read file: " + filepath)
	}

	total_len := len(dataBytes)
	nb_packets := int((total_len + 490) / default_size)
	// 0. prepare
	systeminfo_que(socket)

	// 1. ready
	prepare_up_image(socket, w, h, nb_packets)

	// // time.Sleep(time.Millisecond * 100)

	// // 2. upload
	left_bytes := total_len
	upimg(socket, left_bytes, nb_packets, dataBytes)
	// ////////////////////////////////////
	// // 3. query
	recv_query(socket, nb_packets)
	// // 4. EXECUTE_INFER: 4505
	// exec_infer(socket, nb_packets)
	// // 5. 4506
	// infer_query(socket, nb_packets)
}

func test_wakeup() {
	socket, err := net.DialUDP("udp", nil, &net.UDPAddr{
		IP:   ip_add,
		Port: PORT,
	})
	if err != nil {
		fmt.Println("conect udp server failed,err: ", err)
		return
	}
	defer socket.Close()
	idx := 1020
	tosend := make([]byte, 1024)
	binary.LittleEndian.PutUint32(tosend[:4], uint32(0x55AA55AA))
	binary.LittleEndian.PutUint32(tosend[4:8], uint32(bytesSize))
	binary.LittleEndian.PutUint32(tosend[8:12], uint32(0x450B))
	binary.LittleEndian.PutUint32(tosend[12:16], 0)
	binary.LittleEndian.PutUint32(tosend[16:20], 1) // command counter

	check_sum := GetChecksumByte(tosend[8:idx])
	binary.LittleEndian.PutUint32(tosend[1020:1024], check_sum)

	aaa := make([]byte, 2048)
	recvbuf := bytes.NewBuffer(aaa)
	socket.Write(tosend)
	socket.Read(recvbuf.Bytes())
	ackCheckSumCmp(recvbuf.Bytes(), 0x450B)
	codeCountCpm(recvbuf.Bytes(), 1)

	socket.Read(recvbuf.Bytes())
	replyCheckSumCmp(recvbuf.Bytes(), 0x450B)
	codeCountCpm(recvbuf.Bytes(), 1)
}

func test_sleep() {
	socket, err := net.DialUDP("udp", nil, &net.UDPAddr{
		IP:   ip_add,
		Port: PORT,
	})
	if err != nil {
		fmt.Println("conect udp server failed,err: ", err)
		return
	}
	defer socket.Close()
	idx := 1020
	tosend := make([]byte, 1024)
	binary.LittleEndian.PutUint32(tosend[:4], uint32(0x55AA55AA))
	binary.LittleEndian.PutUint32(tosend[4:8], uint32(bytesSize))
	binary.LittleEndian.PutUint32(tosend[8:12], uint32(0x450A))
	binary.LittleEndian.PutUint32(tosend[12:16], 0)
	binary.LittleEndian.PutUint32(tosend[16:20], 1) // command counter

	check_sum := GetChecksumByte(tosend[8:idx])
	binary.LittleEndian.PutUint32(tosend[1020:1024], check_sum)

	aaa := make([]byte, 2048)
	recvbuf := bytes.NewBuffer(aaa)
	socket.Write(tosend)
	socket.Read(recvbuf.Bytes())
	ackCheckSumCmp(recvbuf.Bytes(), 0x450A)
	codeCountCpm(recvbuf.Bytes(), 1)

	socket.Read(recvbuf.Bytes())
	replyCheckSumCmp(recvbuf.Bytes(), 0x450A)
	codeCountCpm(recvbuf.Bytes(), 1)
}

func main() {
	/////////////////////////////////////////////
	for i := 0; i < 10000; i++ {
		start := time.Now()
		test_send_img_file_bytes("../val2017/164254.jpg", true, imgId)
		fmt.Println("")
		fmt.Println(time.Since(start))
		fmt.Println("")
		imgId++
	}

	/////////////////////////////////////////////
	// GenerateUpgradeFile("./udp_infer2.ota")
	// test_send_file("./upgrade2.bin")
	// test_local_exec()
	// test_send_file("udp_infer0009.zip")

	/////////////////////////////////////////////
	// test_wakeup()
	// test_sleep()
}
