/*
 * @Author: Yinjie Lee
 * @Date: 2022-09-08 21:02:56
 * @LastEditors: Yinjie Lee
 * @LastEditTime: 2022-09-22 10:59:46
 */
// Package server High-speed recv server
package server

import (
	"fmt"

	fu "udp_infer2/frameutils"
	ei "udp_infer2/frameutils/execinfer"
	ire "udp_infer2/frameutils/inferresult"
	msp "udp_infer2/frameutils/modulesleep"
	mvu "udp_infer2/frameutils/modulewakeup"
	pgd "udp_infer2/frameutils/prepareupgrade"
	pui "udp_infer2/frameutils/prepareupimg"
	riqc "udp_infer2/frameutils/recvimgquec"
	sif "udp_infer2/frameutils/systeminfo"
	ugc "udp_infer2/frameutils/upgradecmd"
	usq "udp_infer2/frameutils/upgradestatusquery"
	uic "udp_infer2/frameutils/upimgcmd"
	ws "udp_infer2/wakeupsleep"

	"udp_infer2/npuinfo"
	"udp_infer2/version"

	. "udp_infer2/logger"

	"github.com/panjf2000/gnet"
	"github.com/panjf2000/gnet/pkg/pool/goroutine"
	"github.com/sirupsen/logrus"
)

// FilePath path for string
type sinServer struct {
	*gnet.EventServer
	pool *goroutine.Pool
}

func (ss *sinServer) OnInitComplete(srv gnet.Server) (action gnet.Action) {
	fmt.Printf("version: %d\n", version.VERSION)
	fmt.Printf("UDP server is listening on %s (multi-cores: %t, loops: %d)\n",
		srv.Addr.String(), srv.Multicore, srv.NumEventLoop)
	return
}

func (ss *sinServer) React(f []byte, c gnet.Conn) (out []byte, action gnet.Action) {
	if !fu.ResHeadValid(f) {
		Logger.WithFields(logrus.Fields{
			"CMD cur":  fmt.Sprintf("0x%X", fu.CommandHead),
			"CMD recv": f,
		}).Errorln("包头校验错误")
		return
	}
	err := mvu.RunModuleWakeup(f, c)
	if err != nil {
		Logger.Errorln(err)
		return
	}
	err = msp.RunModuleSleep(f, c)
	if err != nil {
		Logger.Errorln(err)
		return
	}

	if !ws.IsRunning() {
		return
	}

	err = sif.RunSystemInfoCmd(f, c)
	if err != nil {
		Logger.Errorln(err)
		return
	}

	err = pui.RunPrepareUpImgCmd(f, c)
	if err != nil {
		Logger.Errorln(err)
		return
	}
	err = uic.RunUpImgCmd(f)
	if err != nil {
		Logger.Errorln(err)
		return
	}
	err = riqc.RunUpImgQuery(f, c)
	if err != nil {
		Logger.Errorln(err)
		return
	}
	err = ei.RunExecInferCmd(f, c)
	if err != nil {
		Logger.Errorln(err)
		return
	}
	err = ire.RunInferResQuery(f, c)
	if err != nil {
		Logger.Errorln(err)
		return
	}

	err = pgd.RunPrepareUpgrade(f, c)
	if err != nil {
		Logger.Errorln(err)
		return
	}

	err = ugc.RunUpgradeCmd(f, c)
	if err != nil {
		Logger.Errorln(err)
		return
	}

	err = usq.RunUpgradeStatusQuery(f, c)
	if err != nil {
		Logger.Errorln(err)
		return
	}

	return
}

func (ss *sinServer) OnClosed(c gnet.Conn, err error) (action gnet.Action) {
	fmt.Println("UDP server is closed")
	return
}

// ServerRun run gnet server
func ServerRun() {
	npuinfo.NpuTemperature()
	multicore, reuseport := true, true
	ss := new(sinServer)
	gnet.WithSocketRecvBuffer(1024 * 1024 * 10)
	logrus.Fatal(gnet.Serve(ss, "udp://0.0.0.0:37009", gnet.WithMulticore(multicore), gnet.WithReusePort(reuseport)))
}
