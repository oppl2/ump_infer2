MODULE   = $(shell $(GO) list -m)
DATE    ?= $(shell date +%FT%T%z)
VERSION ?= $(shell git describe --abbrev=0 --tags 2> /dev/null || cat .version 2> /dev/null || echo v0)
PKGS     = $(or $(PKG),$(shell $(GO) list ./...))
BIN      ?= bin
RELEASE ?= Debug
PREFIX := /usr/local/udp_infer2
# PROTO_PATH ?= "proto/v1"
# PROTO_OUTPUT ?= "shared"

GO      = go
TIMEOUT = 15
V = 0
Q = $(if $(filter 1,$V),,@)
M = $(shell if [ "$$(tput colors 2> /dev/null || echo 0)" -ge 8 ]; then printf "\033[34;1m▶\033[0m"; else printf "▶"; fi)

GENERATED = # List of generated files

# EXECUTABLES = protoc conan
# K := $(foreach exec,$(EXECUTABLES),\
#         $(if $(shell which $(exec)),some string,$(error "No $(exec) in PATH,please install $(exec)")))

.PHONY: all
all: lint $(GENERATED) | $(BIN) ; $(info $(M) building executable…) @ ## Build program binary
	$Q CGO_ENABLED=1 CFLAGS="-I/usr/local/include" $(GO) build \
		-tags $(RELEASE) \
		-ldflags '-X $(MODULE)/version.Version=$(VERSION) -X $(MODULE)/version.BuildDate=$(DATE)' \
		-o $(BIN)/$(basename $(MODULE)) main.go

# Tools

$(BIN):
	@mkdir -p $@
$(BIN)/%: | $(BIN) ; $(info $(M) building $(PACKAGE)…)
	$Q env GOBIN=$(abspath $(BIN)) $(GO) install $(PACKAGE)

GOIMPORTS = $(BIN)/goimports
$(BIN)/goimports: PACKAGE=golang.org/x/tools/cmd/goimports@latest

REVIVE = $(BIN)/revive
$(BIN)/revive: PACKAGE=github.com/mgechev/revive@latest

PROTOC_GO_GEN = $(BIN)/protoc-gen-go
$(BIN)/protoc-gen-go: PACKAGE=google.golang.org/protobuf/cmd/protoc-gen-go@latest

GOCOV = $(BIN)/gocov
$(BIN)/gocov: PACKAGE=github.com/axw/gocov/gocov@latest

GOCOVXML = $(BIN)/gocov-xml
$(BIN)/gocov-xml: PACKAGE=github.com/AlekSi/gocov-xml@latest

GOTESTSUM = $(BIN)/gotestsum
$(BIN)/gotestsum: PACKAGE=gotest.tools/gotestsum@latest

# Generate

# Tests

TEST_TARGETS := test-bench test-short test-verbose test-race
.PHONY: $(TEST_TARGETS) check test tests
test-bench:   ARGS=-run=__absolutelynothing__ -bench=. ## Run benchmarks
test-short:   ARGS=-short        ## Run only short tests
test-verbose: ARGS=-v            ## Run tests in verbose mode with coverage reporting
test-race:    ARGS=-race         ## Run tests with race detector
$(TEST_TARGETS): NAME=$(MAKECMDGOALS:test-%=%)
$(TEST_TARGETS): test
check test tests: fmt lint $(GENERATED) | $(GOTESTSUM) ; $(info $(M) running $(NAME:%=% )tests…) @ ## Run tests
	$Q mkdir -p test
	$Q $(GOTESTSUM) --junitfile test/tests.xml -- -timeout $(TIMEOUT)s $(ARGS) $(PKGS)

COVERAGE_MODE = atomic
.PHONY: test-coverage
test-coverage: fmt lint $(GENERATED)
test-coverage: | $(GOCOV) $(GOCOVXML) $(GOTESTSUM) ; $(info $(M) running coverage tests…) @ ## Run coverage tests
	$Q mkdir -p test
	$Q $(GOTESTSUM) -- \
		-coverpkg=$(shell echo $(PKGS) | tr ' ' ',') \
		-covermode=$(COVERAGE_MODE) \
		-coverprofile=test/profile.out $(PKGS)
	$Q $(GO) tool cover -html=test/profile.out -o test/coverage.html
	$Q $(GOCOV) convert test/profile.out | $(GOCOVXML) > test/coverage.xml
	@echo -n "Code coverage: "; \
		echo "scale=1;$$(sed -En 's/^<coverage line-rate="([0-9.]+)".*/\1/p' test/coverage.xml) * 100 / 1" | bc -q

.PHONY: lint
lint: | $(REVIVE) ; $(info $(M) running golint…) @ ## Run golint
	$Q $(REVIVE) -config revive.toml -exclude ./go/... -formatter friendly -set_exit_status ./...

.PHONY: fmt
fmt: | $(GOIMPORTS) ; $(info $(M) running gofmt…) @ ## Run gofmt on all source files
	$Q $(GOIMPORTS) -local $(MODULE) -w $(shell $(GO) list -f '{{$$d := .Dir}}{{range $$f := .GoFiles}}{{printf "%s/%s\n" $$d $$f}}{{end}}{{range $$f := .CgoFiles}}{{printf "%s/%s\n" $$d $$f}}{{end}}{{range $$f := .TestGoFiles}}{{printf "%s/%s\n" $$d $$f}}{{end}}' $(PKGS))

# Misc

.PHONY: clean
clean: ; $(info $(M) cleaning…)	@ ## Cleanup everything
	@rm -rf $(BIN) test $(GENERATED)

.PHONY: help
help:
	@grep -hE '^[ a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | \
		awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-17s\033[0m %s\n", $$1, $$2}'

.PHONY: version
version:
	@echo $(VERSION)

.PHONY: install
install:
	install -d bin $(DESTDIR)$(PREFIX)/bin/
	install -m 755 bin/udp_infer2 $(DESTDIR)$(PREFIX)/bin/
	install -m 755 inferstart.sh $(DESTDIR)$(PREFIX)/bin/
	install -m 655 instances_val2017.json $(DESTDIR)$(PREFIX)/bin
	install -m 655 export.py $(DESTDIR)$(PREFIX)/bin
	install -d output $(DESTDIR)$(PREFIX)/bin/output/
	install -d common $(DESTDIR)$(PREFIX)/bin/common/
	install -d data $(DESTDIR)$(PREFIX)/bin/data/
	install -d models $(DESTDIR)$(PREFIX)/bin/models/
	install -d utils $(DESTDIR)$(PREFIX)/bin/utils/
	cp -r output/* -t $(DESTDIR)$(PREFIX)/bin/output/
	cp -r common/* $(DESTDIR)$(PREFIX)/bin/common/
	cp -r data/* $(DESTDIR)$(PREFIX)/bin/data/
	cp -r models/* $(DESTDIR)$(PREFIX)/bin/models/
	cp -r utils/* $(DESTDIR)$(PREFIX)/bin/utils/
	install -m 755 rc-local-go.service /lib/systemd/system/

.PHONY: uninstall
uninstall:
	rm -rf /usr/local/udp_infer2
