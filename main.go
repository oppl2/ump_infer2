/*
 * @Author: Yinjie Lee
 * @Date: 2022-09-09 21:35:26
 * @LastEditors: Yinjie Lee
 * @LastEditTime: 2022-09-16 23:37:20
 */
// Package main is just an entry point.
package main

import (
	"flag"
	"fmt"
	"os"

	"udp_infer2/npuinfo"
	"udp_infer2/server"
)

func main() {
	if len(os.Args) >= 2 {
		command := os.Args[1]

		flag.CommandLine.Parse(os.Args[2:])
		switch command {
		case "-s":
			ret := npuinfo.NpuInit()
			if ret != 0 {
				os.Exit(-1)
			}
			tempr := npuinfo.NpuTemperature()
			fmt.Println(tempr)
		}
		return
	}
	server.ServerRun()
}
