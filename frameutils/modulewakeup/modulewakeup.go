/*
 * @Author: Yinjie Lee
 * @Date: 2022-09-15 02:57:18
 * @LastEditors: Yinjie Lee
 * @LastEditTime: 2022-09-17 00:15:44
 */
package modulewakeup

import (
	"runtime/debug"
	"time"

	fu "udp_infer2/frameutils"
	ws "udp_infer2/wakeupsleep"

	. "udp_infer2/logger"

	"github.com/panjf2000/gnet"
	"github.com/sirupsen/logrus"
)

var wakeuping bool

func isModuleWakeupCmd(f []byte) bool {
	return fu.ResCodeValue(f) == fu.WAKEUP_AI
}

func getModuleWakeupAck(f []byte, status uint32) []byte {
	r := fu.NewReplyWithRes(f)
	r.SetCodeStatus(status)
	cur_checksum := fu.GetChecksumByte4(r.Code) +
		fu.GetChecksumByte4(status) +
		fu.GetChecksumByte4(r.CodeCount) +
		fu.GetChecksumByte(r.Body[:])
	r.SetCheckSum(cur_checksum)
	return r.ToBytes()
}

func getModuleWakeupReply(f []byte, status uint32) []byte {
	r := fu.NewReplyWithRes(f)
	r.SetCodeStatus(status)
	body := fu.ResBodyValue(f)

	r.SetBody(body)
	cur_checksum := fu.GetChecksumByte4(r.Code) +
		fu.GetChecksumByte4(status) +
		fu.GetChecksumByte4(r.CodeCount) +
		fu.GetChecksumByte(r.Body[:])
	r.SetCheckSum(cur_checksum)
	return r.ToBytes()
}

func RunModuleWakeup(f []byte, c gnet.Conn) error {
	start := time.Now()
	if !isModuleWakeupCmd(f) {
		return nil
	}
	defer func() {
		if r := recover(); r != nil {
			Logger.Errorln("Recovered in RunModuleWakeup", r)
			debug.PrintStack()
		}
	}()
	cur_checksum := fu.ResponseCheckSumCalc(f)
	if fu.ResCheckSumValue(f) != cur_checksum {
		Logger.WithFields(logrus.Fields{
			"CheckSum cur":  cur_checksum,
			"CheckSum recv": fu.ResCheckSumValue(f),
		}).Errorln("校验和错误")
		Logger.Debugln("唤醒 ACK 1", getModuleWakeupAck(f, fu.RECV_OK_AND_FAILED))
		c.SendTo(getModuleWakeupAck(f, fu.RECV_OK_AND_FAILED))
		return c.SendTo(getModuleWakeupReply(f, fu.CODE_RUN_SUCCESS))
	}

	// 回复ask
	if err := c.SendTo(getModuleWakeupAck(f, fu.RECV_OK_AND_PASS)); err != nil {
		Logger.Debugln("唤醒 ACK 2 failed", getModuleWakeupAck(f, fu.RECV_OK_AND_PASS))
		return err
	}
	Logger.Debugln("唤醒 ACK 2 ", getModuleWakeupAck(f, fu.RECV_OK_AND_PASS))

	if wakeuping {
		Logger.Debugln("唤醒 reply 3", getModuleWakeupReply(f, fu.CODE_RUNNING))
		return c.SendTo(getModuleWakeupReply(f, fu.CODE_RUNNING))
	}

	if ws.IsRunning() {
		Logger.Debugln("唤醒 reply 4", getModuleWakeupReply(f, fu.CODE_RUN_SUCCESS))
		return c.SendTo(getModuleWakeupReply(f, fu.CODE_RUN_SUCCESS))
	}
	// 1. 开启python进程
	wakeuping = true
	err := ws.WakeUpInferProc()
	wakeuping = false
	if err != nil {
		Logger.WithField("err", err).Errorln("wackup infer process failed")
		return c.SendTo(getModuleWakeupReply(f, fu.CODE_RUN_SUCCESS))
	}

	Logger.WithFields(logrus.Fields{
		"use time": time.Since(start),
	}).Debugln("唤醒")
	Logger.Debugln("唤醒 Done reply", getModuleWakeupReply(f, fu.CODE_RUN_SUCCESS))
	return c.SendTo(getModuleWakeupReply(f, fu.CODE_RUN_SUCCESS))
}
