package frameutils

// InstructionCode 指令字集合
// INVALID_Code Invalid instruction code
var INVALID_Code uint32

// AI_STATUS_CHECK 查询AI加速模块当前工作状态
var AI_STATUS_CHECK uint32 = 0x4501

// PREPARE_UPLOAD_IMAGE 图像上传准备
var PREPARE_UPLOAD_IMAGE uint32 = 0x4502

// UPLOAD_IMAGE 图像上传
var UPLOAD_IMAGE uint32 = 0x4503

// RECEIVE_IMAGE_STATUS  图像接收查询
var RECEIVE_IMAGE_STATUS uint32 = 0x4504

// EXECUTE_INFER  启动识别
var EXECUTE_INFER uint32 = 0x4505

// RECOGNITION_STATUS  识别结果查询
var INFER_STATUS uint32 = 0x4506

// PREPARE_BINDING_DATA  数据装订准备
var PREPARE_UPGRADE_DATA uint32 = 0x4507

// BINDING_DATA  数据装订
var UPGRADE_DATA uint32 = 0x4508

// BINDING_DATA_STATUS  数据装订状态
var UPGRADE_DATA_STATUS uint32 = 0x4509

// SLEEP_AI 休眠
var SLEEP_AI uint32 = 0x450A

// WAKE_AI 唤醒
var WAKEUP_AI uint32 = 0x450B

// #############################################################################################
type CodeStatus uint32

// CodeStatus 指令字执行状态
const (
	// INVALID_Code_Status Invalid instruction code
	INVALID_Code_Status CodeStatus = 0

	// RECV_OK_AND_PASS 收到指令，校验正确
	RECV_OK_AND_PASS = 0xAA00

	// RECV_OK_AND_FAILED 收到指令，校验失败
	RECV_OK_AND_FAILED = 0xAA44
	// CODE_RUNNING  执行中
	CODE_RUNNING = 0xBB11
	// CODE_RUN_SUCCESS  执行成功
	CODE_RUN_SUCCESS = 0xBB33

	// CODE_RUN_FAILED  执行失败/未知指令
	CODE_RUN_FAILED = 0xBB44
)

const (
	SysVer = uint32(0x55180406) // U180406
	APPVer = uint32(0x00001000)
)

const (
	APP   = uint32(0xaa)
	MODEL = uint32(0xbb)
)

var CommandHead uint32 = 0x55AA55AA
var AckReplyHEAD uint32 = 0xAA55AA55

// error codes
const (
	UnknownCommand = uint32(0x0100)
	// SystemVersion  = uint32(0x031B2)

	// labels
	ValidLabels = uint8(0xA)

	// space
	FoundFreeMemorySpace = uint32(0x5400)
	NoFreeMemorySpace    = uint32(0x5401)

	NoFreeDiskSpace = uint32(0x5402)

	// network
	LargeLostPacketRatio = uint32(0x5101)
	NotFoundDataId       = uint32(0x5201)

	// infer
	LoadingModelOverTime = uint32(0x5301)
	LoadingModelSuccess  = uint32(0x5300)
	NotFoundModelFile    = uint32(0x5302)

	// upgrade
	UpgradeSucess                  = uint32(0x1100)
	UnableUpgradeSinceLoss         = uint32(0x1101)
	UnableUpgradeSinceCheckSum     = uint32(0x1102)
	FoundFreeMemorySpaceForUpgrade = uint32(0x1400)
	UnableUpgradeSinceNoFreeSpace  = uint32(0x1401)
	UnableExcute                   = uint32(0x1402) // new
	UnableReplace                  = uint32(0x1403) // new
	IsRunning                      = uint32(0x1404) // new

	// wake up
	CannotWakeup = uint32(0x5102)
)

type InferStatusEnum int

const (
	InferStatusStop    InferStatusEnum = -1
	InferStatusRunning InferStatusEnum = 1
	InferStatusFinish  InferStatusEnum = 0
)
