/*
 * @Author: Yinjie Lee
 * @Date: 2022-09-10 19:19:13
 * @LastEditors: Yinjie Lee
 * @LastEditTime: 2022-09-17 01:10:16
 */
package inferresult

import (
	"encoding/binary"
	"fmt"
	"runtime/debug"
	"time"

	fu "udp_infer2/frameutils"
	"udp_infer2/infer"
	. "udp_infer2/logger"

	"github.com/panjf2000/gnet"
	"github.com/sirupsen/logrus"
)

func isInferResCmd(f []byte) bool {
	return fu.ResCodeValue(f) == fu.INFER_STATUS
}

func getInferResCmdAck(f []byte, status uint32) []byte {
	r := fu.NewReplyWithRes(f)
	r.SetCodeStatus(status)
	cur_checksum := fu.GetChecksumByte4(r.Code) +
		fu.GetChecksumByte4(status) +
		fu.GetChecksumByte4(r.CodeCount) +
		fu.GetChecksumByte(r.Body[:])
	r.SetCheckSum(cur_checksum)
	return r.ToBytes()
}

func writeInferResultReson(body []byte, r uint32) {
	binary.LittleEndian.PutUint32(body[12:], r)
}

func getInferResCmdResult(f []byte, status uint32, result []float32, width int, height int, reason uint32) []byte {
	r := fu.NewReplyWithRes(f)
	r.SetCodeStatus(status)
	body := fu.RepBodyValue(f)
	writeInferResultReson(body, reason)
	r.SetBody(body)
	if status == fu.CODE_RUN_SUCCESS {
		nb_obj := len(result) / 6.0
		binary.LittleEndian.PutUint16(r.Body[8:10], uint16(nb_obj))
		binary.LittleEndian.PutUint16(r.Body[10:12], 0x33)
		id := 24
		obj_counter := 0
		for i := 0; i < int(nb_obj); i++ {
			cls := result[(i+1)*6-1]
			obj_counter = obj_counter + 1
			binary.LittleEndian.PutUint16(r.Body[id:id+2], uint16(cls))

			id = id + 2
			x := result[i*6+1]
			if x < 1 {
				x = x * float32(width)
			}
			binary.LittleEndian.PutUint16(r.Body[id:id+2], uint16(x))

			id = id + 2
			y := result[i*6+2]
			if y < 1 {
				y = y * float32(height)
			}
			binary.LittleEndian.PutUint16(r.Body[id:id+2], uint16(y))

			id = id + 2
			w := result[i*6+3]
			if w < 1 {
				w = w * float32(width)
			}
			binary.LittleEndian.PutUint16(r.Body[id:id+2], uint16(w))

			id = id + 2
			h := result[i*6+4]
			if h < 1 {
				h = h * float32(height)
			}
			binary.LittleEndian.PutUint16(r.Body[id:id+2], uint16(h))

			id = id + 2
			conf := result[i*6] * 100
			binary.LittleEndian.PutUint16(r.Body[id:id+2], uint16(conf))
			obj_info := fmt.Sprintf("cls = %d, (%d, %d, %d, %d), conf=%d\n", int(cls), int(x), int(y), int(w), int(h), int(conf))
			Logger.Debugln("obj_info =", obj_info)
			Logger.Debugln("obj info =", cls, x, y, w, h, conf, "\tw =", width, " h =", height)
			id = id + 2
		}
	}
	cur_checksum := fu.GetChecksumByte4(r.Code) +
		fu.GetChecksumByte4(status) +
		fu.GetChecksumByte4(r.CodeCount) +
		fu.GetChecksumByte(r.Body[:])
	r.SetCheckSum(cur_checksum)
	return r.ToBytes()
}

func imgIDValue(body []byte) uint32 {
	_ = body[3]
	return uint32(body[0]) | uint32(body[1])<<8 | uint32(body[2])<<16 | uint32(body[3])<<24
}

func moduleIDValue(body []byte) uint32 {
	_ = body[7]
	return uint32(body[4]) | uint32(body[5])<<8 | uint32(body[6])<<16 | uint32(body[7])<<24
}

func RunInferResQuery(f []byte, c gnet.Conn) error {
	start := time.Now()
	if !isInferResCmd(f) {
		return nil
	}
	defer func() {
		if r := recover(); r != nil {
			Logger.Errorln("Recovered in RunInferResQuery", r)
			debug.PrintStack()
		}
	}()
	cur_checksum := fu.ResponseCheckSumCalc(f)
	if fu.ResCheckSumValue(f) != cur_checksum {
		Logger.WithFields(logrus.Fields{
			"CheckSum cur":  cur_checksum,
			"CheckSum recv": fu.ResCheckSumValue(f),
		}).Errorln("校验和错误")
		Logger.Debugln("执行推理结果查询 ACK 1", getInferResCmdAck(f, fu.RECV_OK_AND_FAILED))
		c.SendTo(getInferResCmdAck(f, fu.RECV_OK_AND_FAILED))
		return c.SendTo(getInferResCmdResult(f, fu.CODE_RUN_FAILED, nil, 0, 0, fu.UnknownCommand))
	}
	// 回复ask
	if err := c.SendTo(getInferResCmdAck(f, fu.RECV_OK_AND_PASS)); err != nil {
		Logger.Debugln("执行推理结果查询 ACK 2", getInferResCmdAck(f, fu.RECV_OK_AND_PASS))
		return err
	}
	Logger.WithField("ack2 time", time.Since(start)).Debugln("执行推理结果查询 ACK 2", getInferResCmdAck(f, fu.RECV_OK_AND_PASS))
	body := fu.ResBodyValue(f)
	imgID := imgIDValue(body)
	// module := ModuleIDValue(body)
	img, ok := infer.GetImgValue(imgID)
	if !ok {
		Logger.WithField("imgID", imgID).Errorln("未找到该图像")
		Logger.Debugln("执行推理结果查询 reply 3", getInferResCmdResult(f, fu.CODE_RUN_SUCCESS, nil, 0, 0, fu.NotFoundDataId))
		return c.SendTo(getInferResCmdResult(f, fu.CODE_RUN_SUCCESS, nil, 0, 0, fu.NotFoundDataId))
	}
	if infer.GetInferStatus(imgID) {
		Logger.Debugln("执行推理结果查询 reply 4", getInferResCmdResult(f, fu.CODE_RUNNING, nil, 0, 0, fu.NotFoundDataId))
		return c.SendTo(getInferResCmdResult(f, fu.CODE_RUNNING, nil, 0, 0, fu.NotFoundDataId))
	}
	Logger.WithFields(logrus.Fields{
		"imgID":    imgID,
		"count":    fu.RepCountNumValue(f),
		"use time": time.Since(start),
	}).Debugln("执行推理结果查询")
	result, ok := infer.GetResultValue(imgID)
	if !ok {
		Logger.WithFields(logrus.Fields{
			"imgID":    imgID,
			"count":    fu.RepCountNumValue(f),
			"use time": time.Since(start),
		}).Errorln("获取推理结果失败")
		return c.SendTo(getInferResCmdResult(f, fu.CODE_RUN_SUCCESS, result, img.GetImgWidth(), img.GetImgHeight(), fu.NotFoundDataId))
	}
	Logger.Debugln("执行推理结果查询 reply 4", getInferResCmdResult(f, fu.CODE_RUN_SUCCESS, result, img.GetImgWidth(), img.GetImgHeight(), 0))
	return c.SendTo(getInferResCmdResult(f, fu.CODE_RUN_SUCCESS, result, img.GetImgWidth(), img.GetImgHeight(), 0))
}
