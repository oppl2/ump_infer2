/*
 * @Author: Yinjie Lee
 * @Date: 2022-09-09 22:44:44
 * @LastEditors: Yinjie Lee
 * @LastEditTime: 2022-09-21 18:45:40
 */
package upgradecmd

import (
	"archive/zip"
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"runtime/debug"
	"strings"
	"time"

	fu "udp_infer2/frameutils"
	. "udp_infer2/logger"

	"github.com/panjf2000/gnet"
	"github.com/sirupsen/logrus"
)

type Recvb struct {
	m               map[uint32][]byte
	UpgradeFileType uint32 //
	recvCount       uint32 //收包计数
	recvUDPStatus   bool
	upgradeType     uint32
	bytesTotalNum   uint32
	packageSize     uint32
}

var recvBuf Recvb

var ota_file_name = "./udp_infer.zip"
var ota_file_bin = "./ota/udp_infer2.ota"
var ota_file_check = "./ota/udp_infer2.ota.checksum"
var ota_om_file_A = "./output/yolov5s_nms_bs1.om" // 可见光模型文件
var ota_om_file_B = "./output/XXXXX"              // 红外模型文件

var upgradeReason uint32
var upgradeRunning bool

func init() {
	recvBuf.m = make(map[uint32][]byte)
	// recvBuf.noMissMap = nil
	recvBuf.recvCount = 0
	recvBuf.UpgradeFileType = 0
	recvBuf.recvUDPStatus = false
}

func (r *Recvb) GetUpgradeData() []byte {
	bin := make([]byte, 0)
	for i := uint32(1); i < r.recvCount+1; i++ {
		bin = append(bin, r.m[i]...)
	}
	return bin
}

func (r *Recvb) GetRecvCount() uint32 {
	return r.recvCount
}

func (r *Recvb) GetBytesTotalNum() uint32 {
	return r.bytesTotalNum
}

func GetOMVisibleLight() string {
	return ota_om_file_A
}

func GetOMInfrared() string {
	return ota_om_file_B
}

func ResetBuf(bytesTotalNum uint32,
	upgradeFileType uint32) {
	for k := range recvBuf.m {
		recvBuf.m[k] = nil
	}
	recvBuf.bytesTotalNum = bytesTotalNum
	recvBuf.recvCount = 0
	recvBuf.UpgradeFileType = upgradeFileType
	recvBuf.recvUDPStatus = false
}

func isUpgradeCmd(f []byte) bool {
	return fu.ResCodeValue(f) == fu.UPGRADE_DATA
}

func UpgradeReason() uint32 {
	return upgradeReason
}

func UpgradeRunning() bool {
	return upgradeRunning
}

func unzipFile(f *zip.File, destination string) error {
	// 4. Check if file paths are not vulnerable to Zip Slip
	filePath := filepath.Join(destination, f.Name)
	if !strings.HasPrefix(filePath, filepath.Clean(destination)+string(os.PathSeparator)) {
		return fmt.Errorf("invalid file path: %s", filePath)
	}

	// 5. Create directory tree
	if f.FileInfo().IsDir() {
		return os.MkdirAll(filePath, os.ModePerm)
	}

	if err := os.MkdirAll(filepath.Dir(filePath), os.ModePerm); err != nil {
		return err
	}

	// 6. Create a destination file for unzipped content
	destinationFile, err := os.OpenFile(filePath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, f.Mode())
	if err != nil {
		return err
	}
	defer destinationFile.Close()

	// 7. Unzip the content of a file and copy it to the destination file
	zippedFile, err := f.Open()
	if err != nil {
		return err
	}
	defer zippedFile.Close()

	if _, err := io.Copy(destinationFile, zippedFile); err != nil {
		return err
	}
	return nil
}
func unzipSource(source, destination string) error {
	// 1. Open the zip file
	reader, err := zip.OpenReader(source)
	if err != nil {
		return err
	}
	defer reader.Close()

	// 2. Get the absolute destination path
	destination, err = filepath.Abs(destination)
	if err != nil {
		return err
	}

	// 3. Iterate over zip files inside the archive and unzip each of them
	for _, f := range reader.File {
		err := unzipFile(f, destination)
		if err != nil {
			return err
		}
	}

	return nil
}
func startUpgrade() error {
	start := time.Now()
	upgradeRunning = true
	defer func() {
		upgradeRunning = false
	}()
	// total checksum
	recvBuf := GetBufData()
	// body := fu.ResBodyValue(f)
	totalBytesNum := recvBuf.GetBytesTotalNum()

	upgradeData := recvBuf.GetUpgradeData()
	upgrade_type := recvBuf.UpgradeFileType
	if upgrade_type == fu.MODEL {
		ota_file_name = "./output/yolov5s_nms_bs1.om" // TODO 具体实现请修改常量值
	}
	err := ioutil.WriteFile(ota_file_name, upgradeData[:totalBytesNum], 0655)
	if err != nil {
		upgradeReason = fu.UnableUpgradeSinceNoFreeSpace
		Logger.Errorln("replace file error", err)
		return err
	}
	err = unzipSource(ota_file_name, "ota")
	if err != nil {
		Logger.Errorln(err)
		upgradeReason = fu.UnableUpgradeSinceCheckSum
		return err
	}
	//checksum
	checksumSlise, err := ioutil.ReadFile(ota_file_check)
	if err != nil {
		Logger.Errorln("总校验和文件打开失败")
		upgradeReason = fu.UnableUpgradeSinceCheckSum
		return err
	}
	binchecksumSlise, err := ioutil.ReadFile(ota_file_bin)
	if err != nil {
		Logger.Errorln("ota文件打开失败")
		upgradeReason = fu.UnableUpgradeSinceCheckSum
		return err
	}
	binchecksum := fu.GetChecksumByte(binchecksumSlise)
	filechecksum := binary.LittleEndian.Uint32(checksumSlise)
	if binchecksum != filechecksum {
		Logger.WithFields(logrus.Fields{
			"二进制校验和":   binchecksum,
			"接收文件的校验和": filechecksum,
		}).Errorln("总校验和不相等")
		upgradeReason = fu.UnableUpgradeSinceCheckSum
		return errors.New("总校验和不相等")
	}

	// err:= sys.call
	if upgrade_type == fu.APP {
		error_code, err := ExecBin(ota_file_bin)
		if err != nil {
			Logger.Errorln("execute file error", err, error_code)
			upgradeReason = fu.UnableExcute
			return err
		}

	} else if upgrade_type == fu.MODEL {

	}
	upgradeReason = fu.UpgradeSucess
	Logger.WithFields(logrus.Fields{
		"use time": time.Since(start),
	}).Debugln("装订数据验证成功")
	return nil
}

func ExecBin(cmd_str string) (uint32, error) {
	cmd := exec.Command(cmd_str, "-s") // 验证ota程序是否能够执行
	cmd.Stdout = os.Stdout
	err := cmd.Start()
	if err != nil {
		Logger.Errorf("cmd.Start() failed with %s\n", err)
		return fu.UnableExcute, err
	}
	cmd.Process.Signal(os.Interrupt)
	cmd.Process.Wait()
	return 0, nil
}

// Parsing BODY
// 1.
func upgradeTypeValue(body []byte) uint32 {
	_ = body[3]
	return uint32(body[0]) | uint32(body[1])<<8 | uint32(body[2])<<16 | uint32(body[3])<<24
}

// 2.
func upgradeBytesNumValue(body []byte) uint32 {
	_ = body[7]
	return uint32(body[4]) | uint32(body[5])<<8 | uint32(body[6])<<16 | uint32(body[7])<<24
}

// 3.
func upgradePackageTotalNumValue(body []byte) uint32 {
	_ = body[11]
	return uint32(body[8]) | uint32(body[9])<<8 | uint32(body[10])<<16 | uint32(body[11])<<24
}

// 4.
func upgradePackageIdValue(body []byte) uint32 {
	_ = body[15]
	return uint32(body[12]) | uint32(body[13])<<8 | uint32(body[14])<<16 | uint32(body[15])<<24
}

// 5.
func upgradeCurrentSizeValue(body []byte) uint32 {
	_ = body[19]
	return uint32(body[16]) | uint32(body[17])<<8 | uint32(body[18])<<16 | uint32(body[19])<<24
}

// 6.
func upgradeDataValue(body []byte) []byte {
	return body[4*5:]
}

////////////////// communication
func EnableRecvUdpPackage() {
	recvBuf.recvUDPStatus = true
}

func DisableRecvUdpPackage() {
	recvBuf.recvUDPStatus = false
	Logger.Debugln("已禁止ota接收")
}

func RecvUDP() bool {
	return recvBuf.recvUDPStatus
}

func GetBufData() *Recvb {
	return &recvBuf
}

func getUpgradeCmdAck(f []byte, status uint32) []byte {
	r := fu.NewReplyWithRes(f)
	r.SetCodeStatus(status)
	body := fu.ResBodyValue(f)
	r.SetBody(body)
	cur_checksum := fu.GetChecksumByte4(r.Code) +
		fu.GetChecksumByte4(status) +
		fu.GetChecksumByte4(r.CodeCount) +
		fu.GetChecksumByte(r.Body[:])
	r.SetCheckSum(cur_checksum)
	return r.ToBytes()
}

func RunUpgradeCmd(f []byte, c gnet.Conn) error {
	start := time.Now()
	if !isUpgradeCmd(f) {
		return nil
	}
	defer func() {
		if r := recover(); r != nil {
			Logger.Errorln("Recovered in RunUpgradeCmd", r)
			debug.PrintStack()
		}
	}()
	// checksum
	cur_checksum := fu.ResCheckSumValue(f)
	if fu.ResCheckSumValue(f) != cur_checksum {
		Logger.WithFields(logrus.Fields{
			"CheckSum cur":  cur_checksum,
			"CheckSum recv": fu.ResCheckSumValue(f),
		}).Errorln("校验和错误")
		Logger.Debugln("数据装订 ACK 1", getUpgradeCmdAck(f, fu.RECV_OK_AND_FAILED))
		return c.SendTo(getUpgradeCmdAck(f, fu.RECV_OK_AND_FAILED))
	}

	// check prepare
	if !RecvUDP() {
		return errors.New("请先执行数据装订准备指令")
	}
	// 2. 回复ack
	if err := c.SendTo(getUpgradeCmdAck(f, fu.RECV_OK_AND_PASS)); err != nil {
		Logger.Debugln("装订准备 ACK 2 failed", getUpgradeCmdAck(f, fu.RECV_OK_AND_PASS))
		return err
	}
	//
	body := fu.ResBodyValue(f)
	upgradeType := upgradeTypeValue(body)
	if upgradeType != recvBuf.UpgradeFileType {
		Logger.WithFields(logrus.Fields{
			"upgradeType":             upgradeType,
			"recvBuf.UpgradeFileType": recvBuf.UpgradeFileType,
		}).Errorln("数据类型不匹配")
	}
	packageId := upgradePackageIdValue(body)
	recvBuf.m[packageId] = append(recvBuf.m[packageId], upgradeDataValue(body)...)
	if recvBuf.recvCount == 1 {
		recvBuf.packageSize = upgradeCurrentSizeValue(body)
	}
	recvBuf.recvCount++
	// recvBuf.noMissMap[upgradePackageIdValue(body)] = true
	if recvBuf.recvCount == upgradePackageTotalNumValue(body) {
		//开始装订
		Logger.Debugln("开始装订任务")
		go startUpgrade()
	}
	if recvBuf.recvCount%1000 == 0 {
		Logger.WithFields(logrus.Fields{
			"package id":   upgradePackageIdValue(body),
			"upgrade type": upgradeType,
			"count":        fu.RepCountNumValue(f),
			"use time":     time.Since(start),
		}).Debugln("执行数据装订")
	}
	return nil
}
