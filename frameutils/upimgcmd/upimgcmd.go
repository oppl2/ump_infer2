/*
 * @Author: Yinjie Lee
 * @Date: 2022-09-09 22:44:44
 * @LastEditors: Yinjie Lee
 * @LastEditTime: 2022-09-17 00:16:14
 */
package upimgcmd

import (
	"errors"
	"runtime/debug"
	"sync"
	"time"

	fu "udp_infer2/frameutils"
	. "udp_infer2/logger"

	"github.com/sirupsen/logrus"
)

type Recvb struct {
	m               map[uint16][]byte
	imgID           uint32
	recvCount       uint32 //收包计数
	noMissMap       []bool
	recvUDPStatus   bool
	width           uint16
	height          uint16
	packageTotal    uint16
	defaultDataSize uint16
}

var recvBuf Recvb
var mutex sync.Mutex

func (r *Recvb) Merge(src *Recvb) error {
	if r.imgID != src.imgID {
		return errors.New("Merge failed, imgID not equal")
	}
	mutex.Lock()
	for k, v := range src.m {
		r.m[k] = v
	}
	mutex.Unlock()
	r.recvCount += src.recvCount
	for i, v := range src.noMissMap {
		r.noMissMap[i] = v
	}
	return nil
}

func init() {
	recvBuf.m = make(map[uint16][]byte)
	recvBuf.noMissMap = nil
	recvBuf.recvCount = 0
	recvBuf.imgID = 0
	recvBuf.recvUDPStatus = false
}

func (r *Recvb) GetImgData() []byte {
	dasize := r.GetImgDefaultDataSize()
	img2 := make([]byte, r.GetImgBodyBytes()*2)
	nilS := make([]byte, dasize)
	if !r.noMissMap[1] {
		init128 := make([]byte, dasize)
		for i := 0; i < dasize; i++ {
			init128[i] = 128
		}
		copy(img2, init128)
	}
	mutex.Lock()
	for i, v := range r.noMissMap[1:] {
		if v {
			copy(img2[i*dasize:], r.m[uint16(i)])
		} else {
			copy(img2[i*dasize:], nilS)
		}
	}
	mutex.Unlock()
	return img2
}

func (r *Recvb) GetImgBodyBytes() (ret uint32) {
	return uint32(r.packageTotal) * uint32(r.defaultDataSize)
}

func (r *Recvb) GetImgID() uint32 {
	return r.imgID
}

func (r *Recvb) GetImgWidth() int {
	return int(r.width)
}

func (r *Recvb) GetImgHeight() int {
	return int(r.height)
}

func (r *Recvb) GetImgPackageTotal() int {
	return int(r.packageTotal)
}

func (r *Recvb) GetImgDefaultDataSize() int {
	return int(r.defaultDataSize)
}

func (r *Recvb) GetRecvCount() uint32 {
	return r.recvCount
}

func (r *Recvb) GetMissing() []bool {
	return r.noMissMap
}

func (r *Recvb) GetMissingLen() int {
	miss := 0
	for _, nomiss := range r.noMissMap {
		if !nomiss {
			miss++
		}
	}
	return miss
}

func (r *Recvb) WriteMissing(b []byte) []byte {
	missingPackageID := 8
	for i, nomiss := range r.noMissMap {
		if !nomiss {
			b[0+missingPackageID] = byte(i)
			b[1+missingPackageID] = byte(i >> 8)
			missingPackageID = missingPackageID + 2
		}
	}
	return b
}

func ResetBuf(packCount uint16, width uint16, height uint16, totalPackage uint16, defatultDataSize uint16, imgID uint32) {
	// for k := range recvBuf.m {
	// 	recvBuf.m[k] = nil
	// }
	recvBuf.noMissMap = make([]bool, uint64(packCount))
	recvBuf.recvCount = 0
	recvBuf.imgID = imgID
	// recvBuf.reciveNum = 2
	recvBuf.recvUDPStatus = false
	recvBuf.height = height
	recvBuf.width = width
	recvBuf.packageTotal = totalPackage
	recvBuf.defaultDataSize = defatultDataSize
}

func isUpImgCmd(f []byte) bool {
	return fu.ResCodeValue(f) == fu.UPLOAD_IMAGE
}

func imgIDValue(body []byte) uint32 {
	_ = body[3]
	return uint32(body[0]) | uint32(body[1])<<8 | uint32(body[2])<<16 | uint32(body[3])<<24
}

func imgCountValue(body []byte) uint16 {
	_ = body[5]
	return uint16(body[4]) | uint16(body[5])<<8
}

func imgPackageIDValue(body []byte) uint16 {
	_ = body[7]
	return uint16(body[6]) | uint16(body[7])<<8
}

func imgCurSizeValue(body []byte) uint16 {
	_ = body[9]
	return uint16(body[8]) | uint16(body[9])<<8
}

func imgPackageValue(body []byte) []byte {
	r := make([]byte, len(body))
	copy(r, body[10:])
	return r
}

func EnableRecvUdpPackage() {
	recvBuf.recvUDPStatus = true
}

func DisableRecvUdpPackage() {
	recvBuf.recvUDPStatus = false
	Logger.Debugln("已禁止图像接收")
}

func RecvUDP() bool {
	return recvBuf.recvUDPStatus
}

func GetBufDataDeepCopy() *Recvb {
	rc := new(Recvb)
	rc.m = make(map[uint16][]byte)
	for k, v := range recvBuf.m {
		rc.m[k] = v
	}
	rc.noMissMap = nil
	rc.noMissMap = append(rc.noMissMap, recvBuf.noMissMap...)
	rc.recvCount = recvBuf.recvCount
	rc.imgID = recvBuf.imgID
	rc.recvUDPStatus = recvBuf.recvUDPStatus
	rc.width = recvBuf.width
	rc.height = recvBuf.height
	rc.defaultDataSize = recvBuf.defaultDataSize
	rc.packageTotal = recvBuf.packageTotal
	return rc
}
func GetBufData() *Recvb {
	return &recvBuf
}

func RunUpImgCmd(f []byte) error {
	start := time.Now()
	if !isUpImgCmd(f) {
		return nil
	}
	defer func() {
		if r := recover(); r != nil {
			Logger.Errorln("Recovered in RunUpImgCmd", r)
			debug.PrintStack()
		}
	}()
	// 不需要校验，直接收
	if !recvBuf.recvUDPStatus {
		return errors.New("请先运行图片上传准备指令")
	}

	// recvBuf[ImgPackageIDValue(f)] = ImgPackageValue(f)
	body := fu.ResBodyValue(f)
	recvBuf.m[imgPackageIDValue(body)] = imgPackageValue(body)
	recvBuf.recvCount++
	recvBuf.noMissMap[imgPackageIDValue(body)] = true
	if recvBuf.recvCount%1000 == 0 {
		Logger.WithFields(logrus.Fields{
			"package id": imgPackageIDValue(body),
			"imgID":      recvBuf.imgID,
			"count":      fu.RepCountNumValue(f),
			"use time":   time.Since(start),
		}).Debugln("执行图像接收")
	}
	return nil
}
