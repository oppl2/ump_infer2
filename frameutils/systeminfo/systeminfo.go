package systeminfo

import (
	"encoding/binary"
	"errors"
	"os"
	"runtime/debug"
	"time"

	fu "udp_infer2/frameutils"
	ugc "udp_infer2/frameutils/upgradecmd"
	"udp_infer2/npuinfo"
	"udp_infer2/version"
	ws "udp_infer2/wakeupsleep"

	. "udp_infer2/logger"

	ghw "github.com/jaypipes/ghw"
	"github.com/panjf2000/gnet"
	"github.com/shirou/gopsutil/disk"
	"github.com/sirupsen/logrus"
)

type replyBody struct {
	osversion   uint32
	softversion uint32
	exectime    uint32
	selfcheck   uint16
	aistatus    uint16
	tempr       uint32
	labels      []byte
}

const labelMaxNum = 10

/////////////////////////////////////////////////

// disk usage of path/disk
func DiskUsage(path string) uint64 {
	s, _ := disk.Usage(path)
	return s.Free / 1024 / 1024 // Mb
}

func MemStat() int {
	//自身占用
	memory, err := ghw.Memory()
	if err != nil {
		logrus.Errorln("Error getting memory info: %v", err)
	}
	// phys := memory.TotalPhysicalBytes
	usable := memory.TotalUsableBytes / 1024 / 1024
	return int(usable)
}

func sysCheck() (uint32, error) {
	/////////////////////////////////////////////////////////////////////
	/// 1. model file
	vi := ugc.GetOMVisibleLight()
	_, err := os.Stat(vi)
	if err != nil {
		return fu.NotFoundModelFile, errors.New("Not found model visiable light file")
	}
	// inf := ugc.GetOMInfrared()
	// _, err = os.Stat(inf)
	// if err != nil {
	// 	return fu.NotFoundModelFile, errors.New("Not found model infrared file")
	// }
	/// 2. memory
	memFreeSpace := MemStat()
	if memFreeSpace < 3000 {
		return fu.NoFreeMemorySpace, errors.New("No Free Memory Space")
	}
	/// 3. space
	diskFreeSpace := DiskUsage("/")
	if diskFreeSpace < 300 {
		return fu.NoFreeDiskSpace, errors.New("No Free Disk Space")
	}
	return 0, nil
}

/////////////////////////////////////////////////

func init() {
	ret := npuinfo.NpuInit()
	if ret != 0 {
		Logger.Errorln("NPU device init error", ret)
	}
}

func isUpImgQueryCmd(f []byte) bool {
	return fu.ResCodeValue(f) == fu.AI_STATUS_CHECK
}

func getLabelCountValue(body []byte) uint8 {
	return body[0]
}

func getLabelValue(body []byte) []byte {
	return body[4 : getLabelCountValue(body)+4]
}

func getSystemInfoAck(f []byte, status uint32) []byte {
	r := fu.NewReplyWithRes(f)
	r.SetCodeStatus(status)
	cur_checksum := fu.GetChecksumByte4(r.Code) +
		fu.GetChecksumByte4(status) +
		fu.GetChecksumByte4(r.CodeCount) +
		fu.GetChecksumByte(r.Body[:])
	r.SetCheckSum(cur_checksum)
	return r.ToBytes()
}

func getSystemInfoReply(f []byte, status uint32, rpb *replyBody) []byte {
	r := fu.NewReplyWithRes(f)
	r.SetCodeStatus(status)
	body := fu.ResBodyValue(f)
	if rpb != nil {
		binary.LittleEndian.PutUint32(body[0:4], rpb.osversion)
		binary.LittleEndian.PutUint32(body[4:8], rpb.softversion)
		binary.LittleEndian.PutUint32(body[8:12], rpb.exectime)
		binary.LittleEndian.PutUint16(body[12:14], rpb.selfcheck)
		binary.LittleEndian.PutUint16(body[14:16], rpb.aistatus)
		binary.LittleEndian.PutUint32(body[16:20], rpb.tempr)
		copy(body[20:], rpb.labels)
	}
	r.SetBody(body)
	cur_checksum := fu.GetChecksumByte4(r.Code) +
		fu.GetChecksumByte4(status) +
		fu.GetChecksumByte4(r.CodeCount) +
		fu.GetChecksumByte(r.Body[:])
	r.SetCheckSum(cur_checksum)
	return r.ToBytes()
}

func RunSystemInfoCmd(f []byte, c gnet.Conn) error {
	start := time.Now()
	if !isUpImgQueryCmd(f) {
		return nil
	}
	defer func() {
		if r := recover(); r != nil {
			Logger.Errorln("Recovered in RunSystemInfoCmd", r)
			debug.PrintStack()
		}
	}()
	cur_checksum := fu.ResponseCheckSumCalc(f)
	if fu.ResCheckSumValue(f) != cur_checksum {
		Logger.WithFields(logrus.Fields{
			"CheckSum cur":  cur_checksum,
			"CheckSum recv": fu.ResCheckSumValue(f),
		}).Errorln("校验和错误")
		Logger.Debugln("系统状态查询 ACK 1", getSystemInfoAck(f, fu.RECV_OK_AND_FAILED))
		c.SendTo(getSystemInfoAck(f, fu.RECV_OK_AND_FAILED))
		return c.SendTo(getSystemInfoReply(f, fu.CODE_RUN_FAILED, nil))
	}
	// 回复ask
	if err := c.SendTo(getSystemInfoAck(f, fu.RECV_OK_AND_PASS)); err != nil {
		Logger.Debugln("系统状态查询 ACK 2 failed", getSystemInfoAck(f, fu.RECV_OK_AND_PASS))
		return err
	}
	body := fu.ResBodyValue(f)

	// 信息记录
	reply := new(replyBody)
	reply.osversion = fu.SysVer
	reply.exectime = uint32(time.Since(fu.StartTime).Milliseconds())
	reply.softversion = uint32(version.VERSION)
	scv, err := sysCheck()
	if err != nil {
		Logger.WithField("err", err).Errorln("程序自检失败")
	}
	reply.selfcheck = uint16(scv) // TODO 自检结果
	if ws.IsRunning() {
		reply.aistatus = 0x33 // ai状态
	} else {
		reply.aistatus = 0x44 // ai状态
	}
	np := time.Now()
	reply.tempr = uint32(npuinfo.NpuTemperature())
	Logger.Debugln("查询 npu温度 时间", time.Since(np))

	//labels 记录原始数据
	labelsNum := body[0]
	reply.labels = make([]byte, labelMaxNum+1)
	for i := byte(0); i < labelsNum+1; i++ {
		reply.labels[i] = body[i]
	}
	// 添加标签状态
	for i := byte(1); i <= labelsNum; i++ {
		if reply.labels[i] < fu.ValidLabels {
			reply.labels = append(reply.labels, 0x11)
			continue
		}
		reply.labels = append(reply.labels, 0x12)
	}

	Logger.WithFields(logrus.Fields{
		"系统版本":     fu.SysVer,
		"执行时间":     reply.exectime,
		"AI状态":     reply.aistatus,
		"npu温度":    reply.tempr,
		"use time": time.Since(start),
	}).Debugln("系统信息查询")
	Logger.Debugln("系统信息查询 Done reply", getSystemInfoReply(f, fu.CODE_RUN_SUCCESS, reply))
	return c.SendTo(getSystemInfoReply(f, fu.CODE_RUN_SUCCESS, reply))
}
