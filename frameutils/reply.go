/*
 * @Author: Yinjie Lee
 * @Date: 2022-09-10 01:07:33
 * @LastEditors: Yinjie Lee
 * @LastEditTime: 2022-09-14 09:00:29
 */
package frameutils

import (
	"bytes"
	"encoding/binary"
)

type ReplyFrame struct {
	Head       uint32
	Length     uint32
	Code       uint32
	CodeStatus uint32
	CodeCount  uint32
	Body       [1000]byte
	Checksum   uint32
}

func NewReplyWithRes(res []byte) *ReplyFrame {
	r := &ReplyFrame{
		Head:      AckReplyHEAD,
		Length:    ResLengthValue(res),
		Code:      ResCodeValue(res),
		CodeCount: ResCountValue(res),
	}
	return r
}

func RepHeadValue(f []byte) uint32 {
	_ = f[3]
	return uint32(f[0]) | uint32(f[1])<<8 | uint32(f[2])<<16 | uint32(f[3])<<24
}
func RepLengthValue(f []byte) uint32 {
	_ = f[7]
	return uint32(f[4]) | uint32(f[5])<<8 | uint32(f[6])<<16 | uint32(f[7])<<24
}

func RepCodeValue(f []byte) uint32 {
	_ = f[11]
	return uint32(f[8]) | uint32(f[9])<<8 | uint32(f[10])<<16 | uint32(f[11])<<24
}

func RepCodeStatusValue(f []byte) uint32 {
	_ = f[15]
	return uint32(f[12]) | uint32(f[13])<<8 | uint32(f[14])<<16 | uint32(f[15])<<24
}
func RepCountNumValue(f []byte) uint32 {
	_ = f[19]
	return uint32(f[16]) | uint32(f[17])<<8 | uint32(f[18])<<16 | uint32(f[19])<<24
}

func RepBodyValue(f []byte) []byte {
	return f[20:1020]
}

func RepCheckSumValue(f []byte) uint32 {
	_ = f[1023]
	return uint32(f[1020]) | uint32(f[1021])<<8 | uint32(f[1022])<<16 | uint32(f[1023])<<24
}

func (r *ReplyFrame) SetCodeStatus(s uint32) {
	r.CodeStatus = s
}
func (r *ReplyFrame) SetBody(b []byte) {
	copy(r.Body[:], b)
}

func (r *ReplyFrame) SetCheckSum(s uint32) {
	r.Checksum = s
}

func (r *ReplyFrame) ToBytes() []byte {
	buf := &bytes.Buffer{}
	binary.Write(buf, binary.LittleEndian, *r)
	return buf.Bytes()
}
