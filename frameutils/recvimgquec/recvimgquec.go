/*
 * @Author: Yinjie Lee
 * @Date: 2022-09-10 12:27:27
 * @LastEditors: Yinjie Lee
 * @LastEditTime: 2022-09-17 00:15:57
 */
package recvimgquec

import (
	"encoding/binary"
	"runtime/debug"
	"time"

	fu "udp_infer2/frameutils"
	uic "udp_infer2/frameutils/upimgcmd"
	inf "udp_infer2/infer"

	. "udp_infer2/logger"

	"github.com/panjf2000/gnet"
	"github.com/sirupsen/logrus"
)

func isUpImgQueryCmd(f []byte) bool {
	return fu.ResCodeValue(f) == fu.RECEIVE_IMAGE_STATUS
}

func imgIDValue(body []byte) uint32 {
	_ = body[3]
	return uint32(body[0]) | uint32(body[1])<<8 | uint32(body[2])<<16 | uint32(body[3])<<24
}

func imgCountValue(body []byte) uint32 {
	_ = body[5]
	return uint32(body[4]) | uint32(body[5])<<8
}

func imgMissCountValue(body []byte) uint32 {
	_ = body[7]
	return uint32(body[6]) | uint32(body[7])<<8
}

func writeImgMissCount(body []byte, c uint32) {
	binary.LittleEndian.PutUint32(body[6:], c)
}

func writeImgMissPakID(body []byte, pid uint16, offset int) {
	binary.LittleEndian.PutUint16(body[8+offset:], pid)
}

func getUpImgQueryAck(f []byte, status uint32) []byte {
	r := fu.NewReplyWithRes(f)
	r.SetCodeStatus(status)
	cur_checksum := fu.GetChecksumByte4(r.Code) +
		fu.GetChecksumByte4(status) +
		fu.GetChecksumByte4(r.CodeCount) +
		fu.GetChecksumByte(r.Body[:])
	r.SetCheckSum(cur_checksum)
	return r.ToBytes()
}
func getUpImgQueryReply(f []byte, status uint32) []byte {
	r := fu.NewReplyWithRes(f)
	r.SetCodeStatus(status)
	body := fu.ResBodyValue(f)
	r.SetBody(body)
	cur_checksum := fu.GetChecksumByte4(r.Code) +
		fu.GetChecksumByte4(status) +
		fu.GetChecksumByte4(r.CodeCount) +
		fu.GetChecksumByte(r.Body[:])
	r.SetCheckSum(cur_checksum)
	return r.ToBytes()
}

func RunUpImgQuery(f []byte, c gnet.Conn) error {
	start := time.Now()
	if !isUpImgQueryCmd(f) {
		return nil
	}
	defer func() {
		if r := recover(); r != nil {
			Logger.Errorln("Recovered in RunUpImgQuery", r)
			debug.PrintStack()
		}
	}()
	cur_checksum := fu.ResponseCheckSumCalc(f)
	if fu.ResCheckSumValue(f) != cur_checksum {
		Logger.WithFields(logrus.Fields{
			"CheckSum cur":  cur_checksum,
			"CheckSum recv": fu.ResCheckSumValue(f),
		}).Errorln("校验和错误")
		Logger.Debugln("图像接收查询 ACK 1", getUpImgQueryAck(f, fu.RECV_OK_AND_FAILED))
		c.SendTo(getUpImgQueryAck(f, fu.RECV_OK_AND_FAILED))
		return c.SendTo(getUpImgQueryReply(f, fu.CODE_RUN_FAILED))
	}
	// 回复ask

	if err := c.SendTo(getUpImgQueryAck(f, fu.RECV_OK_AND_PASS)); err != nil {
		Logger.Debugln("图像接收查询 ACK 2 failed", getUpImgQueryAck(f, fu.RECV_OK_AND_PASS))
		return err
	}
	Logger.WithField("time sinse", time.Since(start)).Debugln("图像接收查询 ACK 2 ", getUpImgQueryAck(f, fu.RECV_OK_AND_PASS))
	// 关闭图像上传指令
	uic.DisableRecvUdpPackage()
	r := uic.GetBufData()
	// r := uic.GetBufDataDeepCopy()
	// 组织丢包数据帧
	uiqBody := fu.ResBodyValue(f)
	missPc := imgCountValue(uiqBody) - r.GetRecvCount()
	r.GetRecvCount()
	if missPc >= 100 {
		Logger.WithFields(logrus.Fields{
			"miss package count": missPc,
		}).Errorln("CODE_RUN_FAILED,丢包数大于100")
		writeImgMissCount(uiqBody, missPc)
		Logger.Debugln("图像接收查询 Reply 3", getUpImgQueryReply(f, fu.CODE_RUN_SUCCESS))
		return c.SendTo(getUpImgQueryReply(f, fu.CODE_RUN_SUCCESS))
	}

	if missPc >= 1 && missPc <= 100 {
		// 填写实际丢包数
		writeImgMissCount(uiqBody, missPc)
		r.WriteMissing(uiqBody)
		uic.EnableRecvUdpPackage()
		inf.PushImg(r.GetImgID(), uic.GetBufDataDeepCopy())
		Logger.Debugln("图像接收查询 Reply 丢包重传missing", getUpImgQueryReply(f, fu.CODE_RUN_SUCCESS))
		return c.SendTo(getUpImgQueryReply(f, fu.CODE_RUN_SUCCESS))
	}
	// 存储图像
	go inf.PushImg(r.GetImgID(), uic.GetBufDataDeepCopy())
	Logger.WithFields(logrus.Fields{
		"imgID":    imgIDValue(uiqBody),
		"count":    imgCountValue(uiqBody),
		"实际丢包数":    missPc,
		"总包数":      r.GetRecvCount(),
		"总字节数":     r.GetImgBodyBytes(),
		"use time": time.Since(start),
	}).Debugln("执行上传结果查询")
	Logger.Debugln("图像接收查询 Done reply", getUpImgQueryReply(f, fu.CODE_RUN_SUCCESS))
	return c.SendTo(getUpImgQueryReply(f, fu.CODE_RUN_SUCCESS))
}
