/*
 * @Author: Yinjie Lee
 * @Date: 2022-09-10 01:13:57
 * @LastEditors: Yinjie Lee
 * @LastEditTime: 2022-09-13 00:11:39
 */
package frameutils

import . "udp_infer2/logger"

func ResHeadValue(f []byte) uint32 {
	_ = f[3]
	return uint32(f[0]) | uint32(f[1])<<8 | uint32(f[2])<<16 | uint32(f[3])<<24
}

func ResHeadValid(f []byte) bool {
	if len(f) <= 4 {
		Logger.Errorln("数据帧为空")
		return false
	}
	return CmdHeadCmp(f)
}

func ResLengthValue(f []byte) uint32 {
	_ = f[7]
	return uint32(f[4]) | uint32(f[5])<<8 | uint32(f[6])<<16 | uint32(f[7])<<24
}

func ResCodeValue(f []byte) uint32 {
	_ = f[11]
	return uint32(f[8]) | uint32(f[9])<<8 | uint32(f[10])<<16 | uint32(f[11])<<24
}

func ResTmpValue(f []byte) uint32 {
	_ = f[15]
	return uint32(f[12]) | uint32(f[13])<<8 | uint32(f[14])<<16 | uint32(f[15])<<24
}

func ResCountValue(f []byte) uint32 {
	_ = f[19]
	return uint32(f[16]) | uint32(f[17])<<8 | uint32(f[18])<<16 | uint32(f[19])<<24
}

func ResBodyValue(f []byte) []byte {
	return f[20:1020]
}

func ResCheckSumValue(f []byte) uint32 {
	_ = f[1023]
	return uint32(f[1020]) | uint32(f[1021])<<8 | uint32(f[1022])<<16 | uint32(f[1023])<<24
}
