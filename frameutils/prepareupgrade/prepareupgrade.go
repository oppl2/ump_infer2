/*
 * @Author: Yinjie Lee
 * @Date: 2022-09-10 12:27:27
 * @LastEditors: Yinjie Lee
 * @LastEditTime: 2022-09-17 00:15:48
 */
package prepareupgrade

import (
	"runtime/debug"
	"time"

	fu "udp_infer2/frameutils"
	ugc "udp_infer2/frameutils/upgradecmd"

	// inf "udp_infer2/infer"

	. "udp_infer2/logger"

	"github.com/panjf2000/gnet"
	"github.com/sirupsen/logrus"
)

func isPrepareUpgradeCmd(f []byte) bool {
	return fu.ResCodeValue(f) == fu.PREPARE_UPGRADE_DATA
}

// parsing BODY
// 1.
func upgradeTypeValue(body []byte) uint32 {
	_ = body[3]
	return uint32(body[0]) | uint32(body[1])<<8 | uint32(body[2])<<16 | uint32(body[3])<<24
}

// 2.
func upgradeBytesNumValue(body []byte) uint32 {
	_ = body[7]
	return uint32(body[4]) | uint32(body[5])<<8 | uint32(body[6])<<16 | uint32(body[7])<<24
}

func getPrepareUpgradeAck(f []byte, status uint32) []byte {
	r := fu.NewReplyWithRes(f)
	r.SetCodeStatus(status)
	cur_checksum := fu.GetChecksumByte4(r.Code) +
		fu.GetChecksumByte4(status) +
		fu.GetChecksumByte4(r.CodeCount) +
		fu.GetChecksumByte(r.Body[:])
	r.SetCheckSum(cur_checksum)
	return r.ToBytes()
}

func getPrepareUpgradeReply(f []byte, status uint32) []byte {
	r := fu.NewReplyWithRes(f)
	r.SetCodeStatus(status)
	body := fu.ResBodyValue(f)
	r.SetBody(body)
	cur_checksum := fu.GetChecksumByte4(r.Code) +
		fu.GetChecksumByte4(status) +
		fu.GetChecksumByte4(r.CodeCount) +
		fu.GetChecksumByte(r.Body[:])
	r.SetCheckSum(cur_checksum)
	return r.ToBytes()
}

func RunPrepareUpgrade(f []byte, c gnet.Conn) error {
	start := time.Now()
	if !isPrepareUpgradeCmd(f) {
		return nil
	}
	defer func() {
		if r := recover(); r != nil {
			Logger.Errorln("Recovered in RunPrepareUpgrade", r)
			debug.PrintStack()
		}
	}()
	// 1. checksum + ack
	cur_checksum := fu.ResponseCheckSumCalc(f)
	if fu.ResCheckSumValue(f) != cur_checksum {
		Logger.WithFields(logrus.Fields{
			"CheckSum cur":  cur_checksum,
			"CheckSum recv": fu.ResCheckSumValue(f),
		}).Errorln("校验和错误")
		Logger.Debugln("装订准备 ACK 1", getPrepareUpgradeAck(f, fu.RECV_OK_AND_FAILED))
		c.SendTo(getPrepareUpgradeAck(f, fu.RECV_OK_AND_FAILED))
		return c.SendTo(getPrepareUpgradeReply(f, fu.CODE_RUN_FAILED))
	}
	// 2. 回复ack
	if err := c.SendTo(getPrepareUpgradeAck(f, fu.RECV_OK_AND_PASS)); err != nil {
		Logger.Debugln("装订准备 ACK 2 failed", getPrepareUpgradeAck(f, fu.RECV_OK_AND_PASS))
		return err
	}
	Logger.Debugln("装订准备 ACK 2 ", getPrepareUpgradeAck(f, fu.RECV_OK_AND_PASS))

	body := fu.ResBodyValue(f)
	ugc.ResetBuf(upgradeBytesNumValue(body), upgradeTypeValue(body))

	Logger.WithFields(logrus.Fields{
		"数据类型":     upgradeTypeValue(body),
		"数据大小":     upgradeBytesNumValue(body),
		"use time": time.Since(start),
	}).Debugln("执行数据装订准备")
	ugc.EnableRecvUdpPackage()
	Logger.Debugln("装订准备 Done reply", getPrepareUpgradeReply(f, fu.CODE_RUN_SUCCESS))
	return c.SendTo(getPrepareUpgradeReply(f, fu.CODE_RUN_SUCCESS))
}
