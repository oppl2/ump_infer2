/*
 * @Author: Yinjie Lee
 * @Date: 2022-09-10 12:47:06
 * @LastEditors: Yinjie Lee
 * @LastEditTime: 2022-09-17 00:13:38
 */
package execinfer

import (
	"encoding/binary"
	"runtime/debug"
	"time"

	fu "udp_infer2/frameutils"
	inf "udp_infer2/infer"
	. "udp_infer2/logger"

	"github.com/panjf2000/gnet"
	"github.com/sirupsen/logrus"
)

func isExecInferCmd(f []byte) bool {
	return fu.ResCodeValue(f) == fu.EXECUTE_INFER
}

func imgIDValue(body []byte) uint32 {
	_ = body[3]
	return uint32(body[0]) | uint32(body[1])<<8 | uint32(body[2])<<16 | uint32(body[3])<<24
}

func moduleIDValue(body []byte) uint32 {
	_ = body[7]
	return uint32(body[4]) | uint32(body[5])<<8 | uint32(body[6])<<16 | uint32(body[7])<<24
}
func writeInferRunCond(body []byte, c uint32) {
	binary.LittleEndian.PutUint32(body[8:], c)
}

func writeInferRunReson(body []byte, r uint32) {
	binary.LittleEndian.PutUint32(body[12:], r)
}

func getExecInferAck(f []byte, status uint32) []byte {
	r := fu.NewReplyWithRes(f)
	r.SetCodeStatus(status)
	cur_checksum := fu.GetChecksumByte4(r.Code) +
		fu.GetChecksumByte4(status) +
		fu.GetChecksumByte4(r.CodeCount) +
		fu.GetChecksumByte(r.Body[:])
	r.SetCheckSum(cur_checksum)
	return r.ToBytes()
}

func getExecInferReply(f []byte, status uint32, rc uint32, reason uint32) []byte {
	r := fu.NewReplyWithRes(f)
	r.SetCodeStatus(status)
	body := fu.ResBodyValue(f)
	writeInferRunCond(body, rc)
	writeInferRunReson(body, reason)
	r.SetBody(body)
	// if status != fu.CODE_RUNNING {
	// }
	cur_checksum := fu.GetChecksumByte4(r.Code) +
		fu.GetChecksumByte4(status) +
		fu.GetChecksumByte4(r.CodeCount) +
		fu.GetChecksumByte(r.Body[:])
	r.SetCheckSum(cur_checksum)
	r.Checksum = cur_checksum
	return r.ToBytes()
}

func RunExecInferCmd(f []byte, c gnet.Conn) error {
	start := time.Now()
	if !isExecInferCmd(f) {
		return nil
	}
	defer func() {
		if r := recover(); r != nil {
			Logger.Errorln("Recovered in RunExecInferCmd", r)
			debug.PrintStack()
		}
	}()
	cur_checksum := fu.ResponseCheckSumCalc(f)
	if fu.ResCheckSumValue(f) != cur_checksum {
		Logger.WithFields(logrus.Fields{
			"CheckSum cur":  cur_checksum,
			"CheckSum recv": fu.ResCheckSumValue(f),
		}).Errorln("校验和错误")
		Logger.Debugln("启动推理 ACK 1", getExecInferAck(f, fu.RECV_OK_AND_FAILED))
		c.SendTo(getExecInferAck(f, fu.RECV_OK_AND_FAILED))
		return c.SendTo(getExecInferReply(f, fu.CODE_RUN_FAILED, 0x44, fu.UnknownCommand)) // TODO 请填写原因
	}
	// 回复ask
	if err := c.SendTo(getExecInferAck(f, fu.RECV_OK_AND_PASS)); err != nil {
		Logger.Debugln("启动推理 ACK 2", c.SendTo(getExecInferAck(f, fu.RECV_OK_AND_PASS)))
		return err
	}
	body := fu.ResBodyValue(f)
	imgid := imgIDValue(body)

	// 推理启动状态查询
	reason, ok := inf.CheckInferReady(imgid)
	if !ok {
		Logger.Debugln("启动推理失败 reply 3", getExecInferReply(f, fu.CODE_RUN_SUCCESS, 0x44, 0))
		return c.SendTo(getExecInferReply(f, fu.CODE_RUN_SUCCESS, 0x44, reason)) // TODO 请填写原因
	}

	// 开始执行推理
	go inf.RunInfer(imgid)
	Logger.WithFields(logrus.Fields{
		"imgID":    imgid,
		"count":    fu.RepCountNumValue(f),
		"use time": time.Since(start),
	}).Debugln("启动推理")
	Logger.Debugln("启动推理 reply 5", getExecInferReply(f, fu.CODE_RUN_SUCCESS, 0x33, reason))
	return c.SendTo(getExecInferReply(f, fu.CODE_RUN_SUCCESS, 0x33, reason))
}
