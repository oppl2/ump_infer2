/*
 * @Author: Yinjie Lee
 * @Date: 2022-09-10 00:19:57
 * @LastEditors: Yinjie Lee
 * @LastEditTime: 2022-09-17 00:15:52
 */
package prepareupimg

import (
	"runtime/debug"
	"time"

	fu "udp_infer2/frameutils"
	uic "udp_infer2/frameutils/upimgcmd"
	. "udp_infer2/logger"

	"github.com/panjf2000/gnet"
	"github.com/sirupsen/logrus"
)

func isPrepareUpImgCmd(f []byte) bool {
	return fu.ResCodeValue(f) == fu.PREPARE_UPLOAD_IMAGE
}

func imgIDValue(body []byte) uint32 {
	_ = body[3]
	return uint32(body[0]) | uint32(body[1])<<8 | uint32(body[2])<<16 | uint32(body[3])<<24
}

func imgWidthValue(body []byte) uint16 {
	_ = body[5]
	return uint16(body[4]) | uint16(body[5])<<8
}

func imgHeightValue(body []byte) uint16 {
	_ = body[7]
	return uint16(body[6]) | uint16(body[7])<<8
}

func imgTypeValue(body []byte) uint16 {
	_ = body[9]
	return uint16(body[8]) | uint16(body[9])<<8
}

func defaultDataBytesNum(body []byte) uint16 {
	_ = body[11]
	return uint16(body[10]) | uint16(body[11])<<8
}

func packageTotalNum(body []byte) uint16 {
	_ = body[13]
	return uint16(body[12]) | uint16(body[13])<<8
}

// func cleanBody(body []byte) {
// 	for i := 0; i < len(body); i++ {
// 		body[i] = 0
// 	}
// }

func getPreUpICmdAck(f []byte, status uint32) []byte {
	r := fu.NewReplyWithRes(f)
	r.SetCodeStatus(status)
	cur_checksum := fu.GetChecksumByte4(r.Code) +
		fu.GetChecksumByte4(status) +
		fu.GetChecksumByte4(r.CodeCount) +
		fu.GetChecksumByte(r.Body[:])
	r.SetCheckSum(cur_checksum)
	Logger.WithFields(logrus.Fields{
		"code":        r.Code,
		"r.CodeCount": r.CodeCount,
		"CodeStatus":  r.CodeStatus,
		"code count":  f[16:20],
	}).Debugln(" prepare up ask, getPreUpICmdAck")
	return r.ToBytes()
}

func getPreUpICmdReply(f []byte, status uint32) []byte {
	return getPreUpICmdAck(f, status)
}

func RunPrepareUpImgCmd(f []byte, c gnet.Conn) error {
	start := time.Now()
	if !isPrepareUpImgCmd(f) {
		return nil
	}
	defer func() {
		if r := recover(); r != nil {
			Logger.Errorln("Recovered in RunPrepareUpImgCmd", r)
			debug.PrintStack()
		}
	}()
	cur_checksum := fu.ResponseCheckSumCalc(f)
	if fu.ResCheckSumValue(f) != cur_checksum {
		Logger.WithFields(logrus.Fields{
			"CheckSum cur":  cur_checksum,
			"CheckSum recv": fu.ResCheckSumValue(f),
		}).Errorln("校验和错误")
		Logger.Debugln("执行上传准备 ACK 1", getPreUpICmdAck(f, fu.RECV_OK_AND_FAILED))
		c.SendTo(getPreUpICmdAck(f, fu.RECV_OK_AND_FAILED))
		return c.SendTo(getPreUpICmdReply(f, fu.CODE_RUN_FAILED))
	}
	// 回复ask
	if err := c.SendTo(getPreUpICmdAck(f, fu.RECV_OK_AND_PASS)); err != nil {
		Logger.Debugln("执行上传准备 ACK 2", getPreUpICmdAck(f, fu.RECV_OK_AND_PASS))
		return err
	}
	Logger.WithField("ack2 time", time.Since(start)).Debugln("执行上传准备 ACK 2", getPreUpICmdAck(f, fu.RECV_OK_AND_PASS))
	body := fu.ResBodyValue(f)

	// 准备环境

	// 初始化UDP buffer 暂时先使用预开辟的buf
	uic.ResetBuf(packageTotalNum(body)+1, imgWidthValue(body), imgHeightValue(body),
		packageTotalNum(body), defaultDataBytesNum(body), imgIDValue(body))
	// 使能收包
	uic.EnableRecvUdpPackage()
	Logger.WithFields(logrus.Fields{
		"imgID":    imgIDValue(body),
		"count":    fu.RepCountNumValue(f),
		"width":    imgWidthValue(body),
		"height":   imgHeightValue(body),
		"use time": time.Since(start),
		"收包数":      packageTotalNum(body),
		"总字节数":     defaultDataBytesNum(body),
	}).Debugln("执行上传准备")
	Logger.Debugln("执行上传准备 Reply 3", getPreUpICmdReply(f, fu.CODE_RUN_SUCCESS))
	return c.SendTo(getPreUpICmdReply(f, fu.CODE_RUN_SUCCESS))
}
