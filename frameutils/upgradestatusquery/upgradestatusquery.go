/*
 * @Author: Yinjie Lee
 * @Date: 2022-09-10 12:27:27
 * @LastEditors: Yinjie Lee
 * @LastEditTime: 2022-09-17 00:16:08
 */
package upgradestatusquery

import (
	"archive/zip"
	"encoding/binary"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"runtime/debug"
	"strings"
	"time"

	// "time"

	// "syscall"
	// "bytes"
	fu "udp_infer2/frameutils"
	ugc "udp_infer2/frameutils/upgradecmd"

	. "udp_infer2/logger"

	"github.com/panjf2000/gnet"
	"github.com/sirupsen/logrus"
)

func isUpgradeStatusQueryCmd(f []byte) bool {
	return fu.ResCodeValue(f) == fu.UPGRADE_DATA_STATUS
}

// Parsing BODY
// 1.
func upgradeTypeValue(body []byte) uint32 {
	_ = body[3]
	return uint32(body[0]) | uint32(body[1])<<8 | uint32(body[2])<<16 | uint32(body[3])<<24
}

// 2.
func upgradePackageBytesValue(body []byte) uint32 {
	_ = body[7]
	return uint32(body[4]) | uint32(body[5])<<8 | uint32(body[6])<<16 | uint32(body[7])<<24
}

// 3.
func upgradePackageTotalNumValue(body []byte) uint32 {
	_ = body[11]
	return uint32(body[8]) | uint32(body[9])<<8 | uint32(body[10])<<16 | uint32(body[11])<<24
}

// 4.
func upgradeTotalChecksumValue(body []byte, offset int) uint32 {
	_ = body[offset+3]
	return uint32(body[offset]) | uint32(body[offset+1])<<8 | uint32(body[offset+2])<<16 | uint32(body[offset+3])<<24
}

func getUpgradeQueryAck(f []byte, status uint32) []byte {
	r := fu.NewReplyWithRes(f)
	r.SetCodeStatus(status)
	cur_checksum := fu.GetChecksumByte4(r.Code) +
		fu.GetChecksumByte4(status) +
		fu.GetChecksumByte4(r.CodeCount) +
		fu.GetChecksumByte(r.Body[:])
	r.SetCheckSum(cur_checksum)
	return r.ToBytes()
}

func setReason(body []byte, upgradeStatus uint32, reason uint32) {
	binary.LittleEndian.PutUint32(body[12:16], upgradeStatus)
	binary.LittleEndian.PutUint32(body[16:20], reason)
}

func getUpgradeQueryReply(f []byte, status uint32,
	upgradeStatus uint32, // 固化状态
	reason uint32) []byte {
	r := fu.NewReplyWithRes(f)
	r.SetCodeStatus(status)
	body := fu.ResBodyValue(f)
	// 4. / 5.
	setReason(body, upgradeStatus, reason)

	r.SetBody(body)
	cur_checksum := fu.GetChecksumByte4(r.Code) +
		fu.GetChecksumByte4(status) +
		fu.GetChecksumByte4(r.CodeCount) +
		fu.GetChecksumByte(r.Body[:])
	r.SetCheckSum(cur_checksum)
	return r.ToBytes()
}
func unzipFile(f *zip.File, destination string) error {
	// 4. Check if file paths are not vulnerable to Zip Slip
	filePath := filepath.Join(destination, f.Name)
	if !strings.HasPrefix(filePath, filepath.Clean(destination)+string(os.PathSeparator)) {
		return fmt.Errorf("invalid file path: %s", filePath)
	}

	// 5. Create directory tree
	if f.FileInfo().IsDir() {
		return os.MkdirAll(filePath, os.ModePerm)
	}

	if err := os.MkdirAll(filepath.Dir(filePath), os.ModePerm); err != nil {
		return err
	}

	// 6. Create a destination file for unzipped content
	destinationFile, err := os.OpenFile(filePath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, f.Mode())
	if err != nil {
		return err
	}
	defer destinationFile.Close()

	// 7. Unzip the content of a file and copy it to the destination file
	zippedFile, err := f.Open()
	if err != nil {
		return err
	}
	defer zippedFile.Close()

	if _, err := io.Copy(destinationFile, zippedFile); err != nil {
		return err
	}
	return nil
}
func unzipSource(source, destination string) error {
	// 1. Open the zip file
	reader, err := zip.OpenReader(source)
	if err != nil {
		return err
	}
	defer reader.Close()

	// 2. Get the absolute destination path
	destination, err = filepath.Abs(destination)
	if err != nil {
		return err
	}

	// 3. Iterate over zip files inside the archive and unzip each of them
	for _, f := range reader.File {
		err := unzipFile(f, destination)
		if err != nil {
			return err
		}
	}

	return nil
}

func RunUpgradeStatusQuery(f []byte, c gnet.Conn) error {
	start := time.Now()
	if !isUpgradeStatusQueryCmd(f) {
		return nil
	}
	defer func() {
		if r := recover(); r != nil {
			Logger.Errorln("Recovered in RunUpgradeStatusQuery", r)
			debug.PrintStack()
		}
	}()
	cur_checksum := fu.ResponseCheckSumCalc(f)
	if fu.ResCheckSumValue(f) != cur_checksum {
		Logger.WithFields(logrus.Fields{
			"CheckSum cur":  cur_checksum,
			"CheckSum recv": fu.ResCheckSumValue(f),
		}).Errorln("校验和错误")
		Logger.Debugln("装订状态查询 ACK 1", getUpgradeQueryAck(f, fu.RECV_OK_AND_FAILED))
		c.SendTo(getUpgradeQueryAck(f, fu.RECV_OK_AND_FAILED))
		return c.SendTo(getUpgradeQueryReply(f, fu.RECV_OK_AND_FAILED, 0x44, fu.UnableUpgradeSinceCheckSum))
	}

	// 回复ask
	if err := c.SendTo(getUpgradeQueryAck(f, fu.RECV_OK_AND_PASS)); err != nil {
		Logger.Debugln("装订状态查询 ACK 2 failed", getUpgradeQueryAck(f, fu.RECV_OK_AND_PASS))
		return err
	}
	Logger.Debugln("装订状态查询 ACK 2 ", getUpgradeQueryAck(f, fu.RECV_OK_AND_PASS))

	// running
	if ugc.UpgradeRunning() {
		Logger.Debugln("装订状态查询 ACK 3", getUpgradeQueryReply(f, fu.CODE_RUNNING, 0x33, 0))
		return c.SendTo(getUpgradeQueryReply(f, fu.CODE_RUNNING, 0x33, 0))
	}

	body := fu.RepBodyValue(f)
	// 关闭上传指令
	ugc.DisableRecvUdpPackage()
	reason := ugc.UpgradeReason()
	if reason != fu.UpgradeSucess {
		Logger.WithFields(logrus.Fields{
			"reason":    reason,
			"reason 16": fmt.Sprintf("0x%X", reason),
		}).Debugln("装订状态查询 Reply 4", getUpgradeQueryAck(f, fu.RECV_OK_AND_PASS))
		return c.SendTo(getUpgradeQueryReply(f, fu.CODE_RUN_SUCCESS, 0x44, reason))
	}

	Logger.WithFields(logrus.Fields{
		"总包数":      upgradePackageTotalNumValue(body),
		"总字节数":     upgradePackageBytesValue(body),
		"use time": time.Since(start),
	}).Debugln("装订状态查询")
	Logger.Debugln("装订状态查询 Done reply", getUpgradeQueryReply(f, fu.CODE_RUN_SUCCESS, 0x33, 0))

	return c.SendTo(getUpgradeQueryReply(f, fu.CODE_RUN_SUCCESS, 0x33, 0))
}
