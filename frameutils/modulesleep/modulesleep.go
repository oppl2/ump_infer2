/*
 * @Author: Yinjie Lee
 * @Date: 2022-09-15 02:57:18
 * @LastEditors: Yinjie Lee
 * @LastEditTime: 2022-09-17 00:13:20
 */
package modulesleep

import (
	"runtime/debug"
	"time"

	fu "udp_infer2/frameutils"
	. "udp_infer2/logger"
	ws "udp_infer2/wakeupsleep"

	"github.com/panjf2000/gnet"
	"github.com/sirupsen/logrus"
)

var sleeping bool

func isModuleSleepCmd(f []byte) bool {
	return fu.ResCodeValue(f) == fu.SLEEP_AI
}

func getModuleSleepAck(f []byte, status uint32) []byte {
	r := fu.NewReplyWithRes(f)
	r.SetCodeStatus(status)
	cur_checksum := fu.GetChecksumByte4(r.Code) +
		fu.GetChecksumByte4(status) +
		fu.GetChecksumByte4(r.CodeCount) +
		fu.GetChecksumByte(r.Body[:])
	r.SetCheckSum(cur_checksum)
	return r.ToBytes()
}

func getModuleSleepReply(f []byte, status uint32) []byte {
	r := fu.NewReplyWithRes(f)
	r.SetCodeStatus(status)
	body := fu.ResBodyValue(f)
	r.SetBody(body)
	cur_checksum := fu.GetChecksumByte4(r.Code) +
		fu.GetChecksumByte4(status) +
		fu.GetChecksumByte4(r.CodeCount) +
		fu.GetChecksumByte(r.Body[:])
	r.SetCheckSum(cur_checksum)
	return r.ToBytes()
}

func RunModuleSleep(f []byte, c gnet.Conn) error {
	start := time.Now()
	if !isModuleSleepCmd(f) {
		return nil
	}
	defer func() {
		if r := recover(); r != nil {
			Logger.Errorln("Recovered in RunModuleSleep", r)
			debug.PrintStack()
		}
	}()
	cur_checksum := fu.ResponseCheckSumCalc(f)
	if fu.ResCheckSumValue(f) != cur_checksum {
		Logger.WithFields(logrus.Fields{
			"CheckSum cur":  cur_checksum,
			"CheckSum recv": fu.ResCheckSumValue(f),
		}).Errorln("校验和错误")
		Logger.Debugln("睡眠 ACK 1", getModuleSleepAck(f, fu.RECV_OK_AND_FAILED))
		c.SendTo(getModuleSleepAck(f, fu.RECV_OK_AND_FAILED))
		return c.SendTo(getModuleSleepReply(f, fu.CODE_RUN_FAILED))
	}

	// 回复ask
	if err := c.SendTo(getModuleSleepAck(f, fu.RECV_OK_AND_PASS)); err != nil {
		Logger.Debugln("睡眠 ACK 2 failed", getModuleSleepAck(f, fu.RECV_OK_AND_PASS))
		return err
	}
	Logger.Debugln("睡眠 ACK 2 ", getModuleSleepAck(f, fu.RECV_OK_AND_PASS))

	if sleeping {
		Logger.Debugln("睡眠 reply 3", getModuleSleepReply(f, fu.CODE_RUNNING))
		return c.SendTo(getModuleSleepReply(f, fu.CODE_RUNNING))
	}

	if !ws.IsRunning() {
		Logger.Debugln("睡眠 reply 4", getModuleSleepReply(f, fu.CODE_RUN_SUCCESS))
		return c.SendTo(getModuleSleepReply(f, fu.CODE_RUN_SUCCESS))
	}
	// 1.关闭python进程
	sleeping = true
	err := ws.SleepInferProc()
	sleeping = false
	if err != nil {
		Logger.WithField("err", err).Errorln("sleep infer process failed")
		return c.SendTo(getModuleSleepReply(f, fu.CODE_RUN_SUCCESS))
	}

	Logger.WithFields(logrus.Fields{
		"use time": time.Since(start),
	}).Debugln("睡眠")
	Logger.Debugln("睡眠 Done reply", getModuleSleepReply(f, fu.CODE_RUN_SUCCESS))
	return c.SendTo(getModuleSleepReply(f, fu.CODE_RUN_SUCCESS))
}
