/*
 * @Author: Yinjie Lee
 * @Date: 2022-09-09 21:56:07
 * @LastEditors: Yinjie Lee
 * @LastEditTime: 2022-09-14 10:14:23
 */
package frameutils

import "time"

var HeadOffset uint32
var LengthOffset uint32
var CodeOffset uint32
var StartTime time.Time

func init() {
	HeadOffset = 0
	LengthOffset = 32
	CodeOffset = 64
	StartTime = time.Now()
}

func AckHeadCmp(f []byte) bool {
	_ = f[3]
	return f[0] == byte(AckReplyHEAD) && f[1] == byte(AckReplyHEAD>>(HeadOffset+8)) &&
		f[2] == byte(AckReplyHEAD>>(HeadOffset+16)) && f[3] == byte(AckReplyHEAD>>(HeadOffset+24))
}

func CmdHeadCmp(f []byte) bool {
	_ = f[3]
	return f[0] == byte(CommandHead) && f[1] == byte(CommandHead>>(HeadOffset+8)) &&
		f[2] == byte(CommandHead>>(HeadOffset+16)) && f[3] == byte(CommandHead>>(HeadOffset+24))
}

func GetChecksumByte(data []byte) uint32 {
	csum := uint32(0)
	length := len(data)
	for i := 0; i < length; i++ { // 4 Bytes
		csum += uint32(data[i])
	}
	return uint32(csum)
}
func GetChecksumByte4(val uint32) uint32 {
	var cur_checksum uint32
	for {
		if val > 0 {
			cur_checksum = cur_checksum + (val & 0xff)
			val = val >> 8
		} else {
			break
		}
	}
	return cur_checksum
}

func ResponseCheckSumCalc(f []byte) uint32 {
	return GetChecksumByte4(ResCodeValue(f)) +
		GetChecksumByte4(ResTmpValue(f)) +
		GetChecksumByte4(ResCountValue(f)) +
		GetChecksumByte(ResBodyValue(f))
}
