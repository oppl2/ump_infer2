package wakeupsleep

import (
	"os"
	"os/exec"
	. "udp_infer2/logger"
)

var running bool
var cmd exec.Cmd

const (
	workpath = "/usr/local/udp_infer2/bin"
	command  = "/usr/local/python3.7.5/bin/python3"
	args     = "common/yolov5_om_zmq.py"
)

func IsRunning() bool {
	return running
}

func WakeUpInferProc() error {
	if running {
		return nil
	}
	cmd = *exec.Command(command, "-u", args)
	cmd.Stdout = Logger.Out
	cmd.Stderr = Logger.Out
	cmd.Env = os.Environ()
	cmd.Dir = workpath
	err := cmd.Start()
	running = true
	return err
}

func SleepInferProc() error {
	if !running {
		return nil
	}
	err := cmd.Process.Signal(os.Interrupt)
	if err != nil {
		return err
	}
	_, err = cmd.Process.Wait()
	running = false
	return err
}
