/*
 * @Author: Yinjie Lee
 * @Date: 2022-09-09 21:41:43
 * @LastEditors: Yinjie Lee
 * @LastEditTime: 2022-09-17 01:47:57
 */
package logger

import (
	"io/ioutil"
	"os"

	"github.com/sirupsen/logrus"
	"github.com/sirupsen/logrus/hooks/writer"
)

func init_logger() {
	Logger.SetReportCaller(true)
	// Logger.SetLevel(logrus.DebugLevel)
	Logger.SetLevel(logrus.ErrorLevel)
	toConsole()
}

func toConsole() {
	logrus.SetFormatter(&logrus.TextFormatter{
		DisableColors: true,
		FullTimestamp: true,
	})
	Logger.SetOutput(ioutil.Discard)
	Logger.AddHook(&writer.Hook{ // Send logs with level higher than warning to stderr
		Writer: os.Stderr,
		LogLevels: []logrus.Level{
			logrus.PanicLevel,
			logrus.FatalLevel,
			logrus.ErrorLevel,
			logrus.WarnLevel,
		},
	})
	Logger.AddHook(&writer.Hook{ // Send info and debug logs to stdout
		Writer: os.Stdout,
		LogLevels: []logrus.Level{
			logrus.InfoLevel,
			logrus.DebugLevel,
		},
	})
}
