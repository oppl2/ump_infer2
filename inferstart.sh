#! /bin/bash
###
 # @Author: Yinjie Lee
 # @Date: 2022-09-17 00:24:47
 # @LastEditors: Yinjie Lee
 # @LastEditTime: 2022-09-17 00:30:24
###
echo "starting go server..."
PREFIX="/usr/local/udp_infer2"
# 确保dpu 初始化程序完成
sleep 10
cd ${PREFIX}/bin
cp -rf ota/* .
nohup ${PREFIX}/bin/udp_infer2.ota >/dev/null 2>&1 < /dev/null &
# nohup /usr/local/python3.7.5/bin/python3 -u common/yolov5_om_zmq.py >> infer.log 2>&1 &
# 确保 AI程序完全启动
sleep 2

if pgrep -x "udp_infer2.ota" > /dev/null
then
    echo "udp_infer2.ota run success"
else
    echo "to run udp_infer2 backup"
    nohup ${PREFIX}/bin/udp_infer2 >/dev/null 2>&1 < /dev/null &
fi
echo "go server started and receiving data"
