# server tools & grpc
from concurrent.futures import ThreadPoolExecutor
from multiprocessing import context
from pydoc import describe

from cv2 import log
# from tkinter.messagebox import NO

# from zmq import device
from obj_detect_pb2 import ObjDetectionResponse
from obj_detect_pb2_grpc import ObjDetectionServiceServicer, add_ObjDetectionServiceServicer_to_server 
import logging
import grpc

# data utilities
from collections import Counter
import numpy as np
import pandas as pd
import acl

import argparse
import os
import sys
from pathlib import Path
import ipdb

import cv2
import torch
import torch.backends.cudnn as cudnn

# FILE = Path(__file__).resolve()
# ROOT = FILE.parents[0]  # YOLOv5 root directory
# if str(ROOT) not in sys.path:
#     sys.path.append(str(ROOT))  # add ROOT to PATH
# ROOT = Path(os.path.relpath(ROOT, Path.cwd()))  # relative
ROOT = Path.cwd()

import os
import cv2
import glob
import json
import argparse
from tqdm import tqdm
import numpy as np
from collections import OrderedDict
from pycocotools.coco import COCO
from pycocotools.cocoeval import COCOeval
from util.acl_net import Net

np.set_printoptions(suppress=True)
neth, netw = 640, 640


class BatchDataLoader:
    def __init__(self, data_path_list: list, batch_size: int):
        self.data_path_list = data_path_list
        self.sample_num = len(data_path_list)
        self.batch_size = batch_size

    def __len__(self):
        return self.sample_num // self.batch_size + int(self.sample_num % self.batch_size > 0)

    @staticmethod
    def read_data(img_path):
        basename = os.path.basename(img_path)
        img0 = cv2.imread(img_path)
        imgh, imgw = img0.shape[:2]
        img = letterbox(img0, new_shape=(neth, netw))[0]  # padding resize
        imginfo = np.array([neth, netw, imgh, imgw], dtype=np.float16)
        return img0, img, imginfo, basename

    def __getitem__(self, item):
        if (item + 1) * self.batch_size <= self.sample_num:
            slice_end = (item + 1) * self.batch_size
            pad_num = 0
        else:
            slice_end = self.sample_num
            pad_num = (item + 1) * self.batch_size - self.sample_num

        img0 = []
        img = []
        img_info = []
        name_list = []
        for path in self.data_path_list[item * self.batch_size:slice_end]:
            i0, x, info, name = self.read_data(path)
            img0.append(i0)
            img.append(x)
            img_info.append(info)
            name_list.append(name)
        valid_num = len(img)
        for _ in range(pad_num):
            img.append(img[0])
            img_info.append(img_info[0])
        return valid_num, name_list, img0, np.stack(img, axis=0), np.stack(img_info, axis=0)


def coco80_to_coco91_class():
    # converts 80-index (val2014/val2017) to 91-index (paper)
    x = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 27, 28, 31, 32, 33, 34,
         35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63,
         64, 65, 67, 70, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 84, 85, 86, 87, 88, 89, 90]
    return x


def letterbox(img, new_shape=(640, 640), color=(114, 114, 114), auto=False, scaleFill=False, scaleup=True):
    # Resize image to a 32-pixel-multiple rectangle https://github.com/ultralytics/yolov3/issues/232
    shape = img.shape[:2]  # current shape [height, width]
    if isinstance(new_shape, int):
        new_shape = (new_shape, new_shape)

    # Scale ratio (new / old)
    r = min(new_shape[0] / shape[0], new_shape[1] / shape[1])
    if not scaleup:  # only scale down, do not scale up (for better test mAP)
        r = min(r, 1.0)

    # Compute padding
    ratio = r, r  # width, height ratios
    new_unpad = int(round(shape[1] * r)), int(round(shape[0] * r))
    dw, dh = new_shape[1] - new_unpad[0], new_shape[0] - new_unpad[1]  # wh padding
    if auto:  # minimum rectangle
        dw, dh = np.mod(dw, 64), np.mod(dh, 64)  # wh padding
    elif scaleFill:  # stretch
        dw, dh = 0.0, 0.0
        new_unpad = (new_shape[1], new_shape[0])
        ratio = new_shape[1] / shape[1], new_shape[0] / shape[0]  # width, height ratios

    dw /= 2  # divide padding into 2 sides
    dh /= 2

    if shape[::-1] != new_unpad:  # resize
        img = cv2.resize(img, new_unpad, interpolation=cv2.INTER_LINEAR)
    top, bottom = int(round(dh - 0.1)), int(round(dh + 0.1))
    left, right = int(round(dw - 0.1)), int(round(dw + 0.1))
    img = cv2.copyMakeBorder(img, top, bottom, left, right, cv2.BORDER_CONSTANT, value=color)  # add border
    return img, ratio, (dw, dh)


def xyxy2xywh(x):
    # convert nx4 boxes from [x1, y1, x2, y2] to [x, y, w, h] where xy1=top-left, xy2=botttom-right
    y = np.copy(x)
    y[:, 0] = (x[:, 0] + x[:, 2]) / 2  # x center
    y[:, 1] = (x[:, 1] + x[:, 3]) / 2  # y center
    y[:, 2] = x[:, 2] - x[:, 0]  # width
    y[:, 3] = x[:, 3] - x[:, 1]  # height
    return y


def read_class_names(ground_truth_json):
    with open(ground_truth_json, 'r') as file:
        content = file.read()
    content = json.loads(content)
    categories = content.get('categories')
    
    names = {}
    for id, category in enumerate(categories):
        category_name = category.get('name')
        if len(category_name.split()) == 2:
            temp = category_name.split()
            category_name = temp[0] + '_' + temp[1]
        names[id] = category_name.strip('\n')
    return names


def draw_bbox(bbox, img0, color, wt, names):
    det_result_str = ''
    for idx, class_id in enumerate(bbox[:, 5]):
        if float(bbox[idx][4] < float(0.05)):
            continue
        img0 = cv2.rectangle(img0, (int(bbox[idx][0]), int(bbox[idx][1])), (int(bbox[idx][2]), int(bbox[idx][3])),
                             color, wt)
        img0 = cv2.putText(img0, str(idx) + ' ' + names[int(class_id)], (int(bbox[idx][0]), int(bbox[idx][1] + 16)),
                           cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 1)
        img0 = cv2.putText(img0, '{:.4f}'.format(bbox[idx][4]), (int(bbox[idx][0] + 64), int(bbox[idx][1] + 16)),
                           cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 1)
        det_result_str += '{} {} {} {} {} {}\n'.format(
            names[bbox[idx][5]], str(bbox[idx][4]), bbox[idx][0], bbox[idx][1], bbox[idx][2], bbox[idx][3])
    return img0


def eval(ground_truth_json, detection_results_json):
    annType = ['segm', 'bbox', 'keypoints']
    annType = annType[1]  # specify type here
    print('Start evaluate *%s* results...' % (annType))
    cocoGt_file = ground_truth_json
    cocoDt_file = detection_results_json
    cocoGt = COCO(cocoGt_file)
    cocoDt = cocoGt.loadRes(cocoDt_file)
    imgIds = cocoGt.getImgIds()
    print('get %d images' % len(imgIds))
    imgIds = sorted(imgIds)
    cocoEval = COCOeval(cocoGt, cocoDt, annType)
    cocoEval.params.imgIds = imgIds
    cocoEval.evaluate()
    cocoEval.accumulate()
    cocoEval.summarize()

    # copy-paste style
    eval_results = OrderedDict()
    metric = annType
    metric_items = [
        'mAP', 'mAP_50', 'mAP_75', 'mAP_s', 'mAP_m', 'mAP_l'
    ]
    coco_metric_names = {
        'mAP': 0,
        'mAP_50': 1,
        'mAP_75': 2,
        'mAP_s': 3,
        'mAP_m': 4,
        'mAP_l': 5,
        'AR@100': 6,
        'AR@300': 7,
        'AR@1000': 8,
        'AR_s@1000': 9,
        'AR_m@1000': 10,
        'AR_l@1000': 11
    }

    for metric_item in metric_items:
        key = f'{metric}_{metric_item}'
        val = float(
            f'{cocoEval.stats[coco_metric_names[metric_item]]:.3f}'
        )
        eval_results[key] = val
    ap = cocoEval.stats[:6]
    eval_results[f'{metric}_mAP_copypaste'] = (
        f'{ap[0]:.3f} {ap[1]:.3f} {ap[2]:.3f} {ap[3]:.3f} {ap[4]:.3f} {ap[5]:.3f}'
    )
    print(dict(eval_results))

device_id = 0
model_path = "./output/yolov5s_nms_bs1.om"

def check_ret(message, ret):
    if ret != 0:
        raise Exception("{} failed ret={}".format(message, ret))

# if config_path and os.path.exists(config_path):
#     print("config path:", config_path)
#     ret = acl.init(config_path)
# else:
    # ret = acl.init()
ret = acl.init()
check_ret("acl.init", ret)
ret = acl.rt.set_device(0)
check_ret("acl.rt.set_device", ret)

acl_context, ret = acl.rt.create_context(0) # DEBUG:
# print("DEBUG: ", acl.rt.get_context(self.device_id), "\n\n\n")
check_ret("acl.rt.create_context", ret)

model_id, ret = acl.mdl.load_from_file(model_path)
check_ret("acl.mdl.load_from_file", ret)

model_desc = acl.mdl.create_desc()

#ipdb.set_trace()
model = Net(device_id=device_id, model_path=model_path, context = acl_context,
            model_desc = model_desc, model_id = model_id)

class ObjDetectionServer(ObjDetectionServiceServicer):
    def __init__(self, model, context):
        # self.args = args
        self.batch_size = 1
        self.visible = True
        self.output_dir = "output"
        self.ground_truth_json = "./instances_val2017.json"
        self.model_path = "./output/yolov5s_nms_bs1.om"
        self.device_id = 0

        self.coco_names = read_class_names(self.ground_truth_json)
        if not os.path.exists(f'{self.output_dir}/img'):
            os.mkdir(f'{self.output_dir}/img')
        self.coco91class = coco80_to_coco91_class()
        self.model = model

    @torch.no_grad()
    def ExecuteObjInfer(self, request=None, context=None):
        # ipdb.set_trace()
        # if request:
        #     logging.info(request.data)
        #     return ObjDetectionResponse(variety="hello")

        imgsz = (640,640)
        det_result_dict = []

        # img_path_list = glob.glob(self.args.img_path + '/*.jpg')
        # img_path_list.sort()
        img_fn = './val2017/1.jpg'
        if request:
            logging.info(request.data)
            img_fn = request.data            

        img_path_list = [str(ROOT/img_fn)]
        dataloader = BatchDataLoader(img_path_list, self.batch_size)
        total_time = 0.0
        it = 0
        # for i in tqdm(range(len(dataloader))):
        dets = []
        for i in range(len(dataloader)):
            it += 1
            valid_num, basename_list, img0_list, img, imginfo = dataloader[i]
            img = img[..., ::-1].transpose(0, 3, 1, 2)  # BGR tp RGB
            image_np = np.array(img, dtype=np.float32)
            image_np_expanded = image_np / 255.0
            img = np.ascontiguousarray(image_np_expanded).astype(np.float16)

            result, dt = self.model([img, imginfo])  # net out, infer time
            # ipdb.set_trace()
            batch_boxout, boxnum = result
            total_time += dt

            for idx in range(valid_num):
                basename = basename_list[idx]
                name, postfix = basename.split('.')
                num_det = int(boxnum[idx][0])
                boxout = batch_boxout[idx][:num_det * 6].reshape(6, -1).transpose().astype(np.float32)  # 6xN -> Nx6
                
                # convert to coco style
                image_id = int(name)
                box = xyxy2xywh(boxout[:, :4])
                box[:, :2] -= box[:, 2:] / 2  # xy center to top-left corner
                # cur_res = []
                for p, b in zip(boxout.tolist(), box.tolist()):
                    lx, ly, _w, _h = b
                    dets.append([round(p[4], 5)] + [int(lx + _w/2), int(ly + _h/2), int(_w), int(_h)] + [int(p[5])])
                    #dets.append([round(p[4], 5)] + [int(x) for x in b] + [int(p[5])])
                    # det_result_dict.append({'image_id': image_id,
                    #                         # 'category_id': self.coco91class[int(p[5])],
                    #                         'category_id':int(p[5]),
                    #                         'bbox': [round(x, 3) for x in b],
                    #                         'score': round(p[4], 5)},
                    #                         )
                # res.append(cur_res)
                if self.visible:
                    img_dw = draw_bbox(boxout, img0_list[idx], (0, 255, 0), 2, self.coco_names)
                    cv2.imwrite(os.path.join(f'{self.output_dir}/img', basename), img_dw)
        res = ""
        if len(dets) > 0:
            dets = np.array(dets)
            # dets = dets[np.lexsort(dets)[::-1],:]
            # ipdb.set_trace()
            for _row in dets:
                _row = _row.tolist()
                res += str(_row)[1:-1].replace(',', '') + " "
            
        # res = np.sort()

        print('model infer average time:{:.3f} ms / {} image'.format(total_time * 1000 / it, self.batch_size))
        print('saveing predictions.json to output/')
        # with open(f'{self.output_dir}/predictions.json', 'w') as f:
        #     json.dump(det_result_dict, f)
        # res = "hello"
        # print('res = ', res)
        # print("det_result_dict = ", det_result_dict)
        print("dets = ", dets)
        return ObjDetectionResponse(variety=res)




if __name__ == '__main__':
    # Creating a gRPC server in Python
    logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s - %(levelname)s - %(message)s',
    )
    parser = argparse.ArgumentParser(description='YoloV5 offline model inference.')
    parser.add_argument('--ground_truth_json', type=str, default="./instances_val2017.json",
                         help='annotation file path')
    parser.add_argument('--img-path', type=str, default="./val2017", help='input images dir')
    parser.add_argument('--model', type=str, default="yolov5s.om", help='om model path')
    parser.add_argument('--batch-size', type=int, default=1, help='om batch size')
    parser.add_argument('--device-id', type=int, default=0, help='device id')
    parser.add_argument('--output-dir', type=str, default='output', help='output path')
    parser.add_argument('--eval', action='store_true', help='compute mAP')
    parser.add_argument('--visible', action='store_true',
                        help='draw detect result at image and save to output/img')
    flags = parser.parse_args()
    
    # od = ObjDetectionServer()
    # od.ExecuteObjInfer()
    # sys.exit()

    server = grpc.server(ThreadPoolExecutor())
    print("server = ", server)

    ob = ObjDetectionServer(model, acl_context)
    add_ObjDetectionServiceServicer_to_server(ob, server)
    print(" passed \n")
    port = 8080
    server.add_insecure_port(f'[::]:{port}')
    server.start()
    logging.info('server ready on port %r', port)
    server.wait_for_termination()

    ### python3 common/yolov5_om_infer.py --img-path=. --model=output/yolov5s_nms_bs1.om --batch-size=1
